package com.tust.dao;

import com.tust.domain.Favor;
import org.springframework.stereotype.Repository;

import java.util.List;

/*数据持久层Dao点赞接口*/
@Repository
public interface FavorDao {
    /**
     * 按用户ID查找点赞记录接口
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按点赞时间降序
     */
    List<Favor> selectFavorByUID(int UID);

    /**
     * 按用户ID和贴文ID查找点赞记录接口
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     */
    List<Favor> selectFavorByUIDArtID(int UID, int ArtID);

    /**
     * 插入点赞记录接口
     * @param favor 要增加的Favor类
     * @return 影响数据库记录条数
     */
    int insertFavor(Favor favor);

    /**
     * 删除点赞记录接口
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     */
    int deleteFavor(int UID, int ArtID);
}
