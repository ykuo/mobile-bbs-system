package com.tust.dao;

import com.tust.domain.Collection;
import org.springframework.stereotype.Repository;

import java.util.List;

/*数据持久层Dao收藏接口*/
@Repository
public interface CollectionDao {
    /**
     * 按用户ID查找收藏记录接口
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按收藏时间降序
     */
    List<Collection> selectCollectionByUID(int UID);

    /**
     * 按用户ID和贴文ID查找收藏记录接口
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     */
    List<Collection> selectCollectionByUIDArtID(int UID, int ArtID);

    /**
     * 插入收藏记录接口
     * @param collection 要增加的Collection类
     * @return 影响数据库记录条数
     */
    int insertCollection(Collection collection);

    /**
     * 删除收藏记录接口
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     */
    int deleteCollection(int UID, int ArtID);
}
