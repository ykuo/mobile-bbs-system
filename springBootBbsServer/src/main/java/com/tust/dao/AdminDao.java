package com.tust.dao;

import com.tust.domain.Admin;
import org.springframework.stereotype.Repository;

import java.util.List;

/*数据持久层Dao管理员接口*/
@Repository
public interface AdminDao {
    /**
     * 按管理员名查找管理员记录接口
     * @param Aname 管理员名
     * @return List类型的记录集合
     * 备注：按管理员ID升序
     */
    List<Admin> selectAdminByAname(String Aname);
}
