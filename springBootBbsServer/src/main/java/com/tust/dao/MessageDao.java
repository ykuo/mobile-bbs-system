package com.tust.dao;

import com.tust.domain.Message;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/*数据持久层Dao消息接口*/
@Repository
public interface MessageDao {
    /**
     * 插入消息记录接口
     * @param message 要增加的Message类
     * @return 影响数据库记录条数
     */
    int insertMessage(Message message);

    /**
     * 按接收方查找消息记录接口
     * @param MsgReceiver 接收方
     * @return List类型的记录集合
     * 备注：按消息发送时间降序
     */
    List<Message> selectMessageByMsgReceiver(int MsgReceiver);

    /**
     * 按消息ID修改消息是否已读接口
     * @param MsgID 消息ID
     * @param MsgIsReaded 是否已读
     * @return 影响数据库记录条数
     * 备注：
     *   未读：MsgIsReaded置0
     *   已读：MsgIsReaded置1
     */
    int updateMessageByMsgIDRead(@Param("MsgID") int MsgID, @Param("MsgIsReaded") int MsgIsReaded);
}
