package com.tust.dao;

import com.tust.domain.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/*数据持久层Dao用户接口*/
@Repository
public interface UserDao {
    /**
     * 查找所有用户记录接口
     * @return List类型的记录集合
     * 备注：按用户ID升序
     */
    List<User> selectAllUser();

    /**
     * 按用户ID查找用户记录接口
     * @param UID 用户ID
     * @return List类型的记录集合
     */
    List<User> selectUserByUID(int UID);

    /**
     * 按用户姓名查找用户记录接口
     * @param Uname 用户姓名
     * @return List类型的记录集合
     * 备注：对用户姓名模糊查询，按UID升序
     */
    List<User> selectUserByUname(String Uname);

    /**
     * 按用户禁言等级查找用户记录接口
     * @param UbanLevel 用户禁言等级
     * @return List类型的记录集合
     * 备注：按UID升序
     */
    List<User> selectUserByUbanLevel(int UbanLevel);

    /**
     * 按用户ID修改禁言状态接口
     * @param UID 用户ID
     * @param UbanLevel 用户禁言等级
     * @param UbanTime 用户禁言到期时间
     * @return 影响数据库记录条数
     * 备注：
     *   禁言：UbanLevel置-1，UbanTime置当前时间+1天
     *   解除：UbanLevel置0，UbanTime置当前时间
     */
    int updateUserBan(@Param("UID") int UID, @Param("UbanLevel") int UbanLevel, @Param("UbanTime") String UbanTime);

    /**
     * 插入用户记录接口
     * @param user 要增加的User类
     * @return 影响数据库记录条数
     */
    int insertUser(User user);

    /**
     * 按用户ID修改用户记录接口
     * @param UID 用户ID
     * @param Uname 用户名
     * @param Upasswd 用户密码
     * @param Ugender 用户性别
     * @param Uint 用户简介
     * @param UfavorPrivacy 用户点赞隐私
     * @param UcollPrivacy 用户收藏隐私
     * @return 影响数据库记录条数
     */
    int updateUserByUID(@Param("UID") int UID, @Param("Uname") String Uname, @Param("Upasswd") String Upasswd, @Param("Ugender") String Ugender, @Param("Uint") String Uint, @Param("UfavorPrivacy") int UfavorPrivacy, @Param("UcollPrivacy") int UcollPrivacy);
}
