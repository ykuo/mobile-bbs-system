package com.tust.dao;

import com.tust.domain.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/*数据持久层Dao评论接口*/
@Repository
public interface CommentDao {
    /**
     * 查找所有评论记录接口
     * @return List类型的记录集合
     * 备注：按评论ID升序
     */
    List<Comment> selectAllComment();

    /**
     * 查找所有待审核评论记录接口
     * @return List类型的记录集合
     * 备注：按评论发布时间降序
     */
    List<Comment> selectAllBeCheckedComment();

    /**
     * 按评论ID查找评论记录接口
     * @param ComID 评论ID
     * @return List类型的记录集合
     */
    List<Comment> selectCommentByComID(int ComID);

    /**
     * 按用户ID查找评论记录接口
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按评论ID升序
     */
    List<Comment> selectCommentByUID(int UID);

    /**
     * 按贴文ID查找评论记录接口
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     * 备注：按评论发布时间降序
     */
    List<Comment> selectCommentByArtID(int ArtID);

    /**
     * 按贴文ID查找审核通过评论记录接口
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     * 备注：按评论发布时间降序
     */
    List<Comment> selectPassCommentByArtID(int ArtID);

    /**
     * 按贴文标题查找评论记录接口
     * @param ArtTitle 贴文标题
     * @return List类型的记录集合
     * 备注：对贴文标题模糊查询，使用多表连接，按评论发布时间降序
     */
    List<Comment> selectCommentByArtTitle(int ArtTitle);

    /**
     * 按用户姓名查找评论记录接口
     * @param Uname 用户姓名
     * @return List类型的记录集合
     * 备注：对用户姓名模糊查询，使用多表连接，按评论ID升序
     */
    List<Comment> selectCommentByUname(String Uname);

    /**
     * 按评论内容查找评论记录接口
     * @param ComContent 评论内容
     * @return List类型的记录集合
     * 备注：对评论内容模糊查询，按评论ID升序
     */
    List<Comment> selectCommentByComContent(String ComContent);

    /**
     * 按评论审核状态查找评论记录接口
     * @param ComStatus 评论审核状态
     * @return List类型的记录集合
     * 备注：按评论时间降序
     */
    List<Comment> selectCommentByComStatus(int ComStatus);

    /**
     * 按评论时间查找评论记录接口
     * @param ComTime 评论时间
     * @return List类型的记录集合
     */
    List<Comment> selectCommentByComTime(String ComTime);

    /**
     * 按贴文ID删除所有评论接口
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     * 备注：删除贴文操作：(这是第4步)
     *   1.贴文文字置'该贴已被删除'，贴文图片均置null
     *   2.按贴文ID查找评论记录得到用户ID
     *   3.按所有用户ID插入消息
     *   4.按贴文ID删除所有评论
     */
    int deleteAllCommentByArtID(int ArtID);

    /**
     * 按评论ID修改评论审核状态接口
     * @param ComID 评论ID
     * @param ComStatus 评论审核状态
     * @return 影响数据库记录条数
     * 备注：
     *   审核通过：ComStatus置1
     *   审核不通过：ComStatus置-1
     */
    int updateCommentByComIDCheck(@Param("ComID") int ComID, @Param("ComStatus") int ComStatus);

    /**
     * 按评论ID修改评论为删除状态接口
     * @param ComID 评论ID
     * @return 影响数据库记录条数
     * 备注：删除评论操作：
     *   1.评论内容置'该评论已被删除'
     *   2.向用户ID发送信息
     */
    int updateCommentByComIDDelete(int ComID);

    /**
     * 插入评论记录接口
     * @param comment 要增加的Comment类
     * @return 影响数据库记录条数
     */
    int insertComment(Comment comment);
}
