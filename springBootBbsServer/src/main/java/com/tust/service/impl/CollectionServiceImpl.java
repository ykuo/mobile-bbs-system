package com.tust.service.impl;

import com.tust.dao.CollectionDao;
import com.tust.domain.Collection;
import com.tust.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*业务层Service收藏实现类*/
@Service
public class CollectionServiceImpl implements CollectionService {
    /*自动注入Dao收藏接口*/
    @Autowired
    private CollectionDao collectionDao;

    /**
     * 按用户ID查找收藏记录实现方法
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按收藏时间降序
     */
    @Override
    public List<Collection> selectCollectionByUID(int UID) {
        return collectionDao.selectCollectionByUID(UID);
    }

    /**
     * 按用户ID和贴文ID查找收藏记录实现方法
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     */
    @Override
    public List<Collection> selectCollectionByUIDArtID(int UID, int ArtID) {
        return collectionDao.selectCollectionByUIDArtID(UID, ArtID);
    }

    /**
     * 插入收藏记录实现方法
     * @param collection 要增加的Collection类
     * @return 影响数据库记录条数
     */
    @Override
    public int insertCollection(Collection collection) {
        return collectionDao.insertCollection(collection);
    }

    /**
     * 删除收藏记录实现方法
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     */
    @Override
    public int deleteCollection(int UID, int ArtID) {
        return collectionDao.deleteCollection(UID, ArtID);
    }
}
