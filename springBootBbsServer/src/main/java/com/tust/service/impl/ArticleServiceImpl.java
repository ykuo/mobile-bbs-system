package com.tust.service.impl;

import com.tust.dao.ArticleDao;
import com.tust.domain.Article;
import com.tust.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*业务层Service贴文实现类*/
@Service
public class ArticleServiceImpl implements ArticleService {
    /*自动注入Dao贴文接口*/
    @Autowired
    private ArticleDao articleDao;

    /**
     * 查找所有贴文记录实现方法
     * @return List类型的记录集合
     * 备注：按贴文ID升序
     */
    @Override
    public List<Article> selectAllArticle() {
        return articleDao.selectAllArticle();
    }

    /**
     * 查找所有待审核贴文记录实现方法
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    @Override
    public List<Article> selectAllBeCheckedArticle() {
        return articleDao.selectAllBeCheckedArticle();
    }

    /**
     * 按贴文ID查找贴文记录实现方法
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     */
    @Override
    public List<Article> selectArticleByArtID(int ArtID) {
        return articleDao.selectArticleByArtID(ArtID);
    }

    /**
     * 按用户ID查找贴文记录实现方法
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按贴文ID升序
     */
    @Override
    public List<Article> selectArticleByUID(int UID) {
        return articleDao.selectArticleByUID(UID);
    }

    /**
     * 按用户姓名查找贴文记录实现方法
     * @param Uname 用户姓名
     * @return List类型的记录集合
     * 备注：对用户姓名模糊查询，使用多表连接，按贴文ID升序
     */
    @Override
    public List<Article> selectArticleByUname(String Uname) {
        return articleDao.selectArticleByUname(Uname);
    }

    /**
     * 按贴文类别查找贴文记录实现方法
     * @param ArtCategory 贴文类别
     * @return List类型的记录集合
     * 备注：按贴文ID升序
     */
    @Override
    public List<Article> selectArticleByArtCategory(String ArtCategory) {
        return articleDao.selectArticleByArtCategory(ArtCategory);
    }

    /**
     * 按贴文标题查找贴文记录实现方法
     * @param ArtTitle 贴文标题
     * @return List类型的记录集合
     * 备注：对贴文标题模糊查询，按贴文ID升序
     */
    public List<Article> selectArticleByArtTitle(String ArtTitle) {
        return articleDao.selectArticleByArtTitle(ArtTitle);
    }

    /**
     * 按贴文文字查找贴文记录实现方法
     * @param ArtContent 贴文文字
     * @return List类型的记录集合
     * 备注：对贴文文字模糊查询，按贴文ID升序
     */
    @Override
    public List<Article> selectArticleByArtContent(String ArtContent) {
        return articleDao.selectArticleByArtContent(ArtContent);
    }

    /**
     * 按贴文审核状态查找贴文记录实现方法
     * @param ArtStatus 贴文审核状态
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    @Override
    public List<Article> selectArticleByArtStatus(int ArtStatus) {
        return articleDao.selectArticleByArtStatus(ArtStatus);
    }

    /**
     * 按贴文发布时间查找贴文记录实现方法
     * @param ArtSubTime 贴文发布时间
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    @Override
    public List<Article> selectArticleByArtSubTime(String ArtSubTime) {
        return articleDao.selectArticleByArtSubTime(ArtSubTime);
    }

    /**
     * 按贴文ID修改贴文审核状态实现方法
     * @param ArtID 贴文ID
     * @param ArtStatus 贴文审核状态
     * @return 影响数据库记录条数
     * 备注：
     *   审核通过：ArtStatus置1
     *   审核不通过：ArtStatus置-1
     */
    @Override
    public int updateArticleByArtIDCheck(int ArtID, int ArtStatus) {
        return articleDao.updateArticleByArtIDCheck(ArtID, ArtStatus);
    }

    /**
     * 按贴文ID修改贴文为删除状态实现方法
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     * 备注：删除步骤：
     *   1.贴文标题和文字置'该贴已被删除'，贴文图片均置null
     *   2.向用户ID发送信息
     */
    @Override
    public int updateArticleByArtIDDelete(int ArtID) {
        return articleDao.updateArticleByArtIDDelete(ArtID);
    }

    /**
     * 插入贴文记录实现方法
     * @param article 要增加的Article类
     * @return 影响数据库记录条数
     */
    @Override
    public int insertArticle(Article article){
        return articleDao.insertArticle(article);
    }

    /**
     * 按贴文类别查找贴文记录实现方法
     * @param ArtCategory 贴文类别
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    @Override
    public List<Article> selectArticleByArtCategoryOrderByArtSubTime(String ArtCategory) {
        return articleDao.selectArticleByArtCategoryOrderByArtSubTime(ArtCategory);
    }
}
