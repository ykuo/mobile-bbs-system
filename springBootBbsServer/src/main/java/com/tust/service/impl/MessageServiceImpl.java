package com.tust.service.impl;

import com.tust.dao.MessageDao;
import com.tust.domain.Message;
import com.tust.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*业务层Service消息实现类*/
@Service
public class MessageServiceImpl implements MessageService {
    /*自动注入Dao管理员接口*/
    @Autowired
    private MessageDao messageDao;

    /**
     * 插入消息记录实现方法
     * @param message 要增加的Message类
     * @return 影响数据库记录条数
     */
    @Override
    public int insertMessage(Message message) {
        return messageDao.insertMessage(message);
    }

    /**
     * 按接收方查找消息记录实现方法
     * @param MsgReceiver 接收方
     * @return List类型的记录集合
     * 备注：按消息发送时间降序
     */
    @Override
    public List<Message> selectMessageByMsgReceiver(int MsgReceiver) {
        return messageDao.selectMessageByMsgReceiver(MsgReceiver);
    }
    
    /**
     * 按消息ID修改消息是否已读实现方法
     * @param MsgID 消息ID
     * @param MsgIsReaded 是否已读
     * @return 影响数据库记录条数
     * 备注：
     *   未读：MsgIsReaded置0
     *   已读：MsgIsReaded置1
     */
    @Override
    public int updateMessageByMsgIDRead(int MsgID, int MsgIsReaded) {
        return messageDao.updateMessageByMsgIDRead(MsgID, MsgIsReaded);
    }
}
