package com.tust.service.impl;

import com.tust.dao.UserDao;
import com.tust.domain.User;
import com.tust.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*业务层Service用户实现类*/
@Service
public class UserServiceImpl implements UserService {
    /*自动注入Dao用户接口*/
    @Autowired
    private UserDao userDao;

    /**
     * 查找所有用户记录实现方法
     * @return List类型的记录集合
     * 备注：按用户ID升序
     */
    @Override
    public List<User> selectAllUser() {
        return userDao.selectAllUser();
    }

    /**
     * 按用户ID查找用户记录实现方法
     * @param UID 用户ID
     * @return List类型的记录集合
     */
    @Override
    public List<User> selectUserByUID(int UID) {
        return userDao.selectUserByUID(UID);
    }

    /**
     * 按用户姓名查找用户记录实现方法
     * @param Uname 用户姓名
     * @return List类型的记录集合
     * 备注：对用户姓名模糊查询，按UID升序
     */
    @Override
    public List<User> selectUserByUname(String Uname) {
        return userDao.selectUserByUname(Uname);
    }

    /**
     * 按用户禁言等级查找用户记录实现方法
     * @param UbanLevel 用户禁言等级
     * @return List类型的记录集合
     * 备注：按UID升序
     */
    @Override
    public List<User> selectUserByUbanLevel(int UbanLevel) {
        return userDao.selectUserByUbanLevel(UbanLevel);
    }

    /**
     * 按用户ID修改禁言状态实现方法
     * @param UID 用户ID
     * @param UbanLevel 用户禁言等级
     * @param UbanTime 用户禁言到期时间
     * @return 影响数据库记录条数
     * 备注：
     *   禁言：UbanLevel置-1，UbanTime置当前时间+1天
     *   解除：UbanLevel置0，UbanTime置当前时间
     */
    @Override
    public int updateUserBan(int UID, int UbanLevel, String UbanTime) {
        return userDao.updateUserBan(UID, UbanLevel, UbanTime);
    }

    /**
     * 插入用户记录实现方法
     * @param user 要增加的User类
     * @return 影响数据库记录条数
     */
    @Override
    public int insertUser(User user) {
        return userDao.insertUser(user);
    }

    /**
     * 按用户ID修改用户记录实现方法
     * @param UID 用户ID
     * @param Uname 用户名
     * @param Upasswd 用户密码
     * @param Ugender 用户性别
     * @param Uint 用户简介
     * @param UfavorPrivacy 用户点赞隐私
     * @param UcollPrivacy 用户收藏隐私
     * @return 影响数据库记录条数
     */
    @Override
    public int updateUserByUID(int UID, String Uname, String Upasswd, String Ugender, String Uint, int UfavorPrivacy, int UcollPrivacy) {
        return userDao.updateUserByUID(UID, Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy);
    }
}
