package com.tust.service.impl;

import com.tust.dao.AdminDao;
import com.tust.domain.Admin;
import com.tust.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*业务层Service管理员实现类*/
@Service
public class AdminServiceImpl implements AdminService {
    /*自动注入Dao管理员接口*/
    @Autowired
    private AdminDao adminDao;

    /**
     * 按管理员名查找管理员记录实现方法
     * @param Aname 管理员名
     * @return List类型的记录集合
     * 备注：按管理员ID升序
     */
    @Override
    public List<Admin> selectAdminByAname(String Aname) {
        return adminDao.selectAdminByAname(Aname);
    }
}
