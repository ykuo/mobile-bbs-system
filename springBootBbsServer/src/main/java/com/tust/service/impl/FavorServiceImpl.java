package com.tust.service.impl;

import com.tust.dao.FavorDao;
import com.tust.domain.Favor;
import com.tust.service.FavorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*业务层Service点赞实现类*/
@Service
public class FavorServiceImpl implements FavorService {
    /*自动注入Dao点赞接口*/
    @Autowired
    private FavorDao favorDao;

    /**
     * 按用户ID查找点赞记录实现方法
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按点赞时间降序
     */
    @Override
    public List<Favor> selectFavorByUID(int UID) {
        return favorDao.selectFavorByUID(UID);
    }

    /**
     * 按用户ID和贴文ID查找点赞记录实现方法
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     */
    @Override
    public List<Favor> selectFavorByUIDArtID(int UID, int ArtID) {
        return favorDao.selectFavorByUIDArtID(UID, ArtID);
    }

    /**
     * 插入点赞记录实现方法
     * @param favor 要增加的Favor类
     * @return 影响数据库记录条数
     */
    @Override
    public int insertFavor(Favor favor) {
        return favorDao.insertFavor(favor);
    }

    /**
     * 删除点赞记录实现方法
     * @param UID 用户ID
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     */
    @Override
    public int deleteFavor(int UID, int ArtID) {
        return favorDao.deleteFavor(UID, ArtID);
    }
}
