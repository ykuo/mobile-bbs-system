package com.tust.service.impl;

import com.tust.dao.CommentDao;
import com.tust.domain.Comment;
import com.tust.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*业务层Service评论实现类*/
@Service
public class CommentServiceImpl implements CommentService {
    /*自动注入Dao评论接口*/
    @Autowired
    private CommentDao commentDao;

    /**
     * 查找所有评论记录实现方法
     * @return List类型的记录集合
     * 备注：按评论ID升序
     */
    @Override
    public List<Comment> selectAllComment() {
        return commentDao.selectAllComment();
    }

    /**
     * 查找所有待审核评论记录实现方法
     * @return List类型的记录集合
     * 备注：按评论发布时间降序
     */
    @Override
    public List<Comment> selectAllBeCheckedComment() {
        return commentDao.selectAllBeCheckedComment();
    }

    /**
     * 按评论ID查找评论记录实现方法
     * @param ComID 评论ID
     * @return List类型的记录集合
     */
    @Override
    public List<Comment> selectCommentByComID(int ComID) {
        return commentDao.selectCommentByComID(ComID);
    }

    /**
     * 按用户ID查找评论记录实现方法
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按评论ID升序
     */
    @Override
    public List<Comment> selectCommentByUID(int UID) {
        return commentDao.selectCommentByUID(UID);
    }

    /**
     * 按贴文ID查找评论记录实现方法
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     * 备注：按评论发布时间降序
     */
    @Override
    public List<Comment> selectCommentByArtID(int ArtID) {
        return commentDao.selectCommentByArtID(ArtID);
    }

    /**
     * 按贴文ID查找审核通过评论记录实现方法
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     * 备注：按评论发布时间降序
     */
    @Override
    public List<Comment> selectPassCommentByArtID(int ArtID) {
        return commentDao.selectPassCommentByArtID(ArtID);
    }

    /**
     * 按贴文标题查找评论记录实现方法
     * @param ArtTitle 贴文标题
     * @return List类型的记录集合
     * 备注：对贴文标题模糊查询，使用多表连接，按评论发布时间降序
     */
    @Override
    public List<Comment> selectCommentByArtTitle(int ArtTitle) {
        return commentDao.selectCommentByArtTitle(ArtTitle);
    }

    /**
     * 按用户姓名查找评论记录实现方法
     * @param Uname 用户姓名
     * @return List类型的记录集合
     * 备注：对用户姓名模糊查询，使用多表连接，按评论ID升序
     */
    @Override
    public List<Comment> selectCommentByUname(String Uname) {
        return commentDao.selectCommentByUname(Uname);
    }

    /**
     * 按评论内容查找评论记录实现方法
     * @param ComContent 评论内容
     * @return List类型的记录集合
     * 备注：对评论内容模糊查询，按评论ID升序
     */
    @Override
    public List<Comment> selectCommentByComContent(String ComContent) {
        return commentDao.selectCommentByComContent(ComContent);
    }

    /**
     * 按评论审核状态查找评论记录实现方法
     * @param ComStatus 评论审核状态
     * @return List类型的记录集合
     * 备注：按评论时间降序
     */
    @Override
    public List<Comment> selectCommentByComStatus(int ComStatus) {
        return commentDao.selectCommentByComStatus(ComStatus);
    }

    /**
     * 按评论时间查找评论记录实现方法
     * @param ComTime 评论时间
     * @return List类型的记录集合
     */
    @Override
    public List<Comment> selectCommentByComTime(String ComTime) {
        return commentDao.selectCommentByComTime(ComTime);
    }

    /**
     * 按贴文ID删除所有评论实现方法
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     * 备注：删除贴文操作：(这是第4步)
     *   1.贴文文字置'该贴已被删除'，贴文图片均置null
     *   2.按贴文ID查找评论记录得到用户ID
     *   3.按所有用户ID插入消息
     *   4.按贴文ID删除所有评论
     */
    @Override
    public int deleteAllCommentByArtID(int ArtID) {
        return commentDao.deleteAllCommentByArtID(ArtID);
    }

    /**
     * 按评论ID修改评论审核状态实现方法
     * @param ComID 评论ID
     * @param ComStatus 评论审核状态
     * @return 影响数据库记录条数
     * 备注：
     *   审核通过：ComStatus置1
     *   审核不通过：ComStatus置-1
     */
    @Override
    public int updateCommentByComIDCheck(int ComID, int ComStatus) {
        return commentDao.updateCommentByComIDCheck(ComID, ComStatus);
    }

    /**
     * 按评论ID修改评论为删除状态实现方法
     * @param ComID 评论ID
     * @return 影响数据库记录条数
     * 备注：删除评论操作：
     *   1.评论内容置'该评论已被删除'
     *   2.向用户ID发送信息
     */
    @Override
    public int updateCommentByComIDDelete(int ComID) {
        return commentDao.updateCommentByComIDDelete(ComID);
    }

    /**
     * 插入评论记录实现方法
     * @param comment 要增加的Comment类
     * @return 影响数据库记录条数
     */
    @Override
    public int insertComment(Comment comment) {
        return commentDao.insertComment(comment);
    }
}
