package com.tust.service;

import com.tust.domain.Admin;

import java.util.List;

/*业务层Service管理员接口*/
public interface AdminService {
    /**
     * 按管理员名查找管理员记录接口
     * @param Aname 管理员名
     * @return List类型的记录集合
     * 备注：按管理员ID升序
     */
    List<Admin> selectAdminByAname(String Aname);
}
