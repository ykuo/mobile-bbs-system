package com.tust.service;

import com.tust.domain.Message;

import java.util.List;

/*业务层Service消息接口*/
public interface MessageService {
    /**
     * 插入消息记录接口
     * @param message 要增加的Message类
     * @return 影响数据库记录条数
     */
    int insertMessage(Message message);

    /**
     * 按接收方查找消息记录接口
     * @param MsgReceiver 接收方
     * @return List类型的记录集合
     * 备注：按消息发送时间降序
     */
    List<Message> selectMessageByMsgReceiver(int MsgReceiver);

    /**
     * 按消息ID修改消息是否已读接口
     * @param MsgID 消息ID
     * @param MsgIsReaded 是否已读
     * @return 影响数据库记录条数
     * 备注：
     *   未读：MsgIsReaded置0
     *   已读：MsgIsReaded置1
     */
    int updateMessageByMsgIDRead(int MsgID, int MsgIsReaded);
}
