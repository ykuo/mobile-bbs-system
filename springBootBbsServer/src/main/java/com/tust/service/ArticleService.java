package com.tust.service;

import com.tust.domain.Article;

import java.util.List;

/*业务层Service贴文接口*/
public interface ArticleService {
    /**
     * 查找所有贴文记录接口
     * @return List类型的记录集合
     * 备注：按贴文ID升序
     */
    List<Article> selectAllArticle();

    /**
     * 查找所有待审核贴文记录接口
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    List<Article> selectAllBeCheckedArticle();

    /**
     * 按贴文ID查找贴文记录接口
     * @param ArtID 贴文ID
     * @return List类型的记录集合
     */
    List<Article> selectArticleByArtID(int ArtID);

    /**
     * 按用户ID查找贴文记录接口
     * @param UID 用户ID
     * @return List类型的记录集合
     * 备注：按贴文ID升序
     */
    List<Article> selectArticleByUID(int UID);

    /**
     * 按用户姓名查找贴文记录接口
     * @param Uname 用户姓名
     * @return List类型的记录集合
     * 备注：对用户姓名模糊查询，使用多表连接，按贴文ID升序
     */
    List<Article> selectArticleByUname(String Uname);

    /**
     * 按贴文类别查找贴文记录接口
     * @param ArtCategory 贴文类别
     * @return List类型的记录集合
     * 备注：按贴文ID升序
     */
    List<Article> selectArticleByArtCategory(String ArtCategory);

    /**
     * 按贴文标题查找贴文记录接口
     * @param ArtTitle 贴文标题
     * @return List类型的记录集合
     * 备注：对贴文标题模糊查询，按贴文ID升序
     */
    List<Article> selectArticleByArtTitle(String ArtTitle);

    /**
     * 按贴文文字查找贴文记录接口
     * @param ArtContent 贴文文字
     * @return List类型的记录集合
     * 备注：对贴文文字模糊查询，按贴文ID升序
     */
    List<Article> selectArticleByArtContent(String ArtContent);

    /**
     * 按贴文审核状态查找贴文记录接口
     * @param ArtStatus 贴文审核状态
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    List<Article> selectArticleByArtStatus(int ArtStatus);

    /**
     * 按贴文发布时间查找贴文记录接口
     * @param ArtSubTime 贴文发布时间
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    List<Article> selectArticleByArtSubTime(String ArtSubTime);

    /**
     * 按贴文ID修改贴文审核状态接口
     * @param ArtID 贴文ID
     * @param ArtStatus 贴文审核状态
     * @return 影响数据库记录条数
     * 备注：
     *   审核通过：ArtStatus置1
     *   审核不通过：ArtStatus置-1
     */
    int updateArticleByArtIDCheck(int ArtID, int ArtStatus);

    /**
     * 按贴文ID修改贴文为删除状态接口
     * @param ArtID 贴文ID
     * @return 影响数据库记录条数
     * 备注：删除步骤：
     *   1.贴文标题和文字置'该贴已被删除'，贴文图片均置null
     *   2.向用户ID发送信息
     */
    int updateArticleByArtIDDelete(int ArtID);

    /**
     * 插入贴文记录接口
     * @param article 要增加的Article类
     * @return 影响数据库记录条数
     */
    int insertArticle(Article article);

    /**
     * 按贴文类别查找贴文记录接口
     * @param ArtCategory 贴文类别
     * @return List类型的记录集合
     * 备注：按贴文发布时间降序
     */
    List<Article> selectArticleByArtCategoryOrderByArtSubTime(String ArtCategory);
}
