package com.tust.controller;

import com.tust.domain.Article;
import com.tust.domain.Favor;
import com.tust.domain.Message;
import com.tust.domain.User;
import com.tust.paging.ArticleListPaging;
import com.tust.service.*;
import com.tust.trans.ArticleList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*控制层Controller点赞类*/
@Controller
public class FavorController {
    /*自动注入Service点赞接口*/
    @Autowired
    private FavorService favorService;
    /*自动注入Service消息接口*/
    @Autowired
    private MessageService messageService;
    /*自动注入Service贴文接口*/
    @Autowired
    private ArticleService articleService;
    /*自动注入Service用户接口*/
    @Autowired
    private UserService userService;
    /*自动注入Service评论接口*/
    @Autowired
    private CommentService commentService;

    /********************************************客户端请求********************************************/

    /*用于分页功能*/
    private List<ArticleList> selectFavorArtLists = null;//存储完整查询结果
    private ArticleListPaging favorArtListPaging = new ArticleListPaging();//分页类

    /**
     * 查询所有点赞贴文列表
     * 响应"/searchAllFavorArtList.do"请求
     * @param UID 用户ID
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/searchAllFavorArtList.do")
    public List<ArticleList> searchAllFavorArtList(int UID) {
        List<Favor> favors = favorService.selectFavorByUID(UID);//按用户ID查找点赞记录

        List<ArticleList> articleLists = new ArrayList<>();
        Article article = null;
        ArticleList articleList = null;
        for (Favor favor: favors) {//按贴文ID查找贴文记录
            article = articleService.selectArticleByArtID(favor.getArtID()).get(0);

            articleList = new ArticleList();
            articleList.setArtID(article.getArtID());
            articleList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
            articleList.setArtCategory(article.getArtCategory());
            articleList.setArtTitle(article.getArtTitle());
            articleList.setArtFavorNum(article.getArtFavorNum());
            articleList.setArtCollNum(article.getArtCollNum());
            articleList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());
            articleList.setArtSubTime(article.getArtSubTime());
            articleList.setArtStatus(-2);

            articleLists.add(articleList);
        }

        selectFavorArtLists = articleLists;//存储完整查询结果
        favorArtListPaging.recordCount = selectFavorArtLists.size();
        favorArtListPaging.pageCount = selectFavorArtLists.size() % favorArtListPaging.maxRecordNum == 0 ? selectFavorArtLists.size() / favorArtListPaging.maxRecordNum : selectFavorArtLists.size() / favorArtListPaging.maxRecordNum + 1;
        favorArtListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有点赞贴文列表: size:" + articleLists.size());
        return favorArtListPaging.showPageContent(selectFavorArtLists);//调用分页显示方法
    }

    /**
     * 查询点赞贴文列表
     * 响应"/searchFavorArtList.do"请求
     * @param UID 用户ID
     * @param searchContent 搜索内容
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/searchFavorArtList.do")
    public List<ArticleList> searchFavorArtList(int UID, String searchContent) {
        List<Favor> favors = favorService.selectFavorByUID(UID);//按用户ID查找点赞记录

        List<ArticleList> articleLists = new ArrayList<>();
        Article article = null;
        ArticleList articleList = null;
        for (Favor favor: favors) {//按贴文ID查找贴文记录
            article = articleService.selectArticleByArtID(favor.getArtID()).get(0);

            if (article.getArtTitle().contains(searchContent)) {
                articleList = new ArticleList();
                articleList.setArtID(article.getArtID());
                articleList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
                articleList.setArtCategory(article.getArtCategory());
                articleList.setArtTitle(article.getArtTitle());
                articleList.setArtFavorNum(article.getArtFavorNum());
                articleList.setArtCollNum(article.getArtCollNum());
                articleList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());
                articleList.setArtSubTime(article.getArtSubTime());
                articleList.setArtStatus(-2);

                articleLists.add(articleList);
            }
        }

        selectFavorArtLists = articleLists;//存储完整查询结果
        favorArtListPaging.recordCount = selectFavorArtLists.size();
        favorArtListPaging.pageCount = selectFavorArtLists.size() % favorArtListPaging.maxRecordNum == 0 ? selectFavorArtLists.size() / favorArtListPaging.maxRecordNum : selectFavorArtLists.size() / favorArtListPaging.maxRecordNum + 1;
        favorArtListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询点赞贴文列表: size:" + articleLists.size());
        return favorArtListPaging.showPageContent(selectFavorArtLists);//调用分页显示方法
    }

    /**
     * 分页下一页
     * 响应"/nextFavorArtListPage.do"请求
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/nextFavorArtListPage.do")
    public List<ArticleList> nextFavorArtListPage() {
        if (!favorArtListPaging.isLastPage(selectFavorArtLists)) {//当前页不是最后一页
            favorArtListPaging.currentPage++;//当前页码+1
        } else return new ArrayList<>();
        return favorArtListPaging.showPageContent(selectFavorArtLists);//调用分页显示方法
    }

    /**
     * 分页最前页
     * 响应"/topFavorArtListPage.do"请求
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/topFavorArtListPage.do")
    public List<ArticleList> topFavorArtListPage() {
        favorArtListPaging.currentPage = 0;//当前页码为0
        return favorArtListPaging.showPageContent(selectFavorArtLists);//调用分页显示方法
    }

    /**
     * 点赞贴文
     * 响应"/favorateArticle.do"请求
     * @param ArtID 贴文ID
     * @param UID 用户ID
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/favorateArticle.do")
    public String favorateArticle(int ArtID, int UID) {
        String msg = null;
        Message message = new Message();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();//获取当前时间
        Favor favor = new Favor(UID, ArtID, formatter.format(date));

        if (favorService.insertFavor(favor) == 1) {//插入点赞成功
            msg ="insertFavorSuccess";

            //发送消息
            List<Article> articles = articleService.selectArticleByArtID(ArtID);//找到贴文
            List<User> users = userService.selectUserByUID(UID);//找到用户
            if (articles.size() == 0 || users.size() == 0) return "databaseFalse";
            message.setMsgReceiver(articles.get(0).getUID());
            message.setArtID(ArtID);
            message.setMsgContent("您的贴文：'" + articles.get(0).getArtTitle() + "'被" + users.get(0).getUname() + "点赞");
            date = new Date();//获取当前时间
            message.setMsgTime(formatter.format(date));
            message.setMsgFromUser(0);
            message.setMsgIsReaded(0);
            if (messageService.insertMessage(message) != 1) return "databaseFalse";//发送消息
        } else msg = "insertFavorFalse";//插入点赞失败

        System.out.println("点赞贴文：" + msg);
        return msg;
    }

    /**
     * 取消点赞贴文
     * 响应"/cancelFavorateArticle.do"请求
     * @param ArtID 贴文ID
     * @param UID 用户ID
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/cancelFavorateArticle.do")
    public String cancelFavorateArticle(int ArtID, int UID) {
        String msg = null;

        if (favorService.deleteFavor(UID, ArtID) == 1) msg ="deleteFavorSuccess";//删除点赞成功
        else msg = "deleteFavorFalse";//删除点赞失败

        System.out.println("取消点赞：" + msg);
        return msg;
    }
}
