package com.tust.controller;

import com.tust.domain.Article;
import com.tust.domain.Message;
import com.tust.domain.User;
import com.tust.paging.ArticleListPaging;
import com.tust.paging.ArticlePaging;
import com.tust.paging.ExploreListPaging;
import com.tust.service.*;
import com.tust.trans.ArticleDetail;
import com.tust.trans.ArticleList;
import com.tust.trans.ExploreList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

/*控制层Controller贴文类*/
@Controller
public class ArticleController {
    /*自动注入Service用户接口*/
    @Autowired
    private UserService userService;
    /*自动注入Service贴文接口*/
    @Autowired
    private ArticleService articleService;
    /*自动注入Service消息接口*/
    @Autowired
    private MessageService messageService;
    /*自动注入Service评论接口*/
    @Autowired
    private CommentService commentService;
    /*自动注入Service点赞接口*/
    @Autowired
    private FavorService favorService;
    /*自动注入Service收藏接口*/
    @Autowired
    private CollectionService collectionService;

    /********************************************管理端请求********************************************/

    /*用于分页功能*/
    private List<Article> selectArticles = null;//存储完整查询结果
    private ArticlePaging articlePaging = new ArticlePaging();//分页类

    private int ifShowAllArticle = 1;//保存当前数据表显示状态(默认显示所有数据)

    /**
     * 查询所有贴文
     * 响应"/selectAllArticle.do"请求
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/selectAllArticle.do")
    public List<Article> selectAllArticle() {
        List<Article> articles = articleService.selectAllArticle();

        selectArticles = articles;//存储完整查询结果
        articlePaging.recordCount = selectArticles.size();
        articlePaging.pageCount = selectArticles.size() % articlePaging.maxRecordNum == 0 ? selectArticles.size() / articlePaging.maxRecordNum : selectArticles.size() / articlePaging.maxRecordNum + 1;
        articlePaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有贴文: size:" + articles.size());
        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 查询所有待审核贴文
     * 响应"/selectAllBeCheckedArticle.do"请求
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/selectAllBeCheckedArticle.do")
    public List<Article> selectAllBeCheckedArticle() {
        List<Article> articles = articleService.selectAllBeCheckedArticle();

        selectArticles = articles;//存储完整查询结果
        articlePaging.recordCount = selectArticles.size();
        articlePaging.pageCount = selectArticles.size() % articlePaging.maxRecordNum == 0 ? selectArticles.size() / articlePaging.maxRecordNum : selectArticles.size() / articlePaging.maxRecordNum + 1;
        articlePaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有待审核贴文: size:" + articles.size());
        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 查询贴文
     * 响应"/selectArticle.do"请求
     * @param selectOption 选择项
     * @param selectContent 选择内容
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/selectArticle.do")
    public List<Article> selectArticle(String selectOption, String selectContent) {
        List<Article> articles = null;

        if (selectOption.equals("贴文ID")) articles = articleService.selectArticleByArtID(Integer.valueOf(selectContent));//按贴文ID查找贴文记录
        else if (selectOption.equals("用户ID")) articles = articleService.selectArticleByUID(Integer.valueOf(selectContent));//按用户ID查找贴文记录
        else if (selectOption.equals("用户名")) articles = articleService.selectArticleByUname(selectContent);//按用户名查找贴文记录
        else if (selectOption.equals("贴文类别")) articles = articleService.selectArticleByArtCategory(selectContent);//按贴文类别查找贴文记录
        else if (selectOption.equals("贴文标题")) articles = articleService.selectArticleByArtTitle(selectContent);//按贴文标题查找贴文记录
        else if (selectOption.equals("贴文文字")) articles = articleService.selectArticleByArtContent(selectContent);//按贴文文字查找贴文记录
        else if (selectOption.equals("审核状态")) articles = articleService.selectArticleByArtStatus(Integer.valueOf(selectContent));//按审核状态查找贴文记录
        else if (selectOption.equals("发布时间")) articles = articleService.selectArticleByArtSubTime(selectContent);//按发布时间查找贴文记录

        selectArticles = articles;//存储完整查询结果
        articlePaging.recordCount = selectArticles.size();
        articlePaging.pageCount = selectArticles.size() % articlePaging.maxRecordNum == 0 ? selectArticles.size() / articlePaging.maxRecordNum : selectArticles.size() / articlePaging.maxRecordNum + 1;
        articlePaging.currentPage = 0;//当前页码为0

        System.out.println("查询贴文: selectOption:" + selectOption + " selectContent:" + selectContent + " size:" + articles.size());
        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 分页上一页
     * 响应"/preArticlePage.do"请求
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/preArticlePage.do")
    public List<Article> preArticlePage() {
        if (!articlePaging.isFirstPage()) {//当前页不是第一页
            articlePaging.currentPage--;//当前页码-1
        }
        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 分页下一页
     * 响应"/nextArticlePage.do"请求
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/nextArticlePage.do")
    public List<Article> nextArticlePage() {
        if (!articlePaging.isLastPage(selectArticles)) {//当前页不是最后一页
            articlePaging.currentPage++;//当前页码+1
        }
        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 分页最前页
     * 响应"/topArticlePage.do"请求
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/topArticlePage.do")
    public List<Article> topArticlePage() {
        articlePaging.currentPage = 0;//当前页码为0
        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 分页最尾页
     * 响应"/endArticlePage.do"请求
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/endArticlePage.do")
    public List<Article> endArticlePage() {
        articlePaging.currentPage = articlePaging.pageCount - 1;//当前页码为总分页数量-1
        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 获取当前分页信息
     * 响应"/getArticlePageInfo.do"请求
     * @param tableStatus 数据表状态
     * @return pageInfo 返回分页信息
     */
    @ResponseBody
    @RequestMapping("/manage/getArticlePageInfo.do")
    public ArticlePaging getArticlePageInfo(int tableStatus) {
        ifShowAllArticle = tableStatus;
        return articlePaging;
    }

    /**
     * 审核通过贴文
     * 响应"/passArticle.do"请求
     * @param ArtID 贴文ID
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/manage/passArticle.do")
    public String passArticle(int ArtID) {
        String msg = null;
        Message message = new Message();

        if (articleService.updateArticleByArtIDCheck(ArtID, 1) == 1) {//审核通过贴文成功
            msg = "passArticleSuccess";

            //发送消息
            List<Article> articles = articleService.selectArticleByArtID(ArtID);//找到贴文
            if (articles.size() == 0) return "databaseFalse";
            message.setMsgReceiver(articles.get(0).getUID());
            message.setArtID(ArtID);
            message.setMsgContent("您的贴文：'" + articles.get(0).getArtTitle() + "'已通过审核");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();//获取当前时间
            message.setMsgTime(formatter.format(date));
            message.setMsgFromUser(0);
            message.setMsgIsReaded(0);
            if (messageService.insertMessage(message) != 1) return "databaseFalse";//发送消息
        } else {//审核通过贴文失败
            msg = "passArticleFalse";
        }

        System.out.println("贴文通过审核: " + msg);
        return msg;
    }

    /**
     * 审核不通过贴文
     * 响应"/notPassArticle.do"请求
     * @param ArtID 贴文ID
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/manage/notPassArticle.do")
    public String notPassArticle(int ArtID) {
        String msg = null;
        Message message = new Message();

        if (articleService.updateArticleByArtIDCheck(ArtID, -1) == 1) {//审核不通过贴文成功
            msg = "notPassArticleSuccess";

            //禁言等级+1
            List<Article> articles = articleService.selectArticleByArtID(ArtID);//找到贴文
            if (articles.size() == 0) return "databaseFalse";
            int UID = articles.get(0).getUID();//获得用户ID
            List<User> users = userService.selectUserByUID(UID);//找到用户
            if (users.size() == 0) return "databaseFalse";
            int UbanLevel = users.get(0).getUbanLevel();//获得禁言等级
            users.get(0).setUbanLevel(UbanLevel + 1);//禁言等级+1

            if (UbanLevel + 1 > 3) {//若禁言等级>3，禁言用户，发送消息
                //禁言时间+1天
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar calendar = new GregorianCalendar();
                Date date = new Date();//获取当前时间
                calendar.setTime(date);
                calendar.add(calendar.DATE,1);//当前时间+1天
                date = calendar.getTime();
                if (userService.updateUserBan(UID, -1, formatter.format(date)) != 1) return "databaseFalse";//禁言时间+1

                //发送消息
                message.setMsgReceiver(UID);
                message.setArtID(-1);
                message.setMsgContent("非常抱歉，您的贴文：" + articles.get(0).getArtTitle() + "未通过审核\n由于您的发言不当，已被禁言1天");
                date = new Date();//获取当前时间
                message.setMsgTime(formatter.format(date));
                message.setMsgFromUser(0);
                message.setMsgIsReaded(0);
            } else {//发送消息
                message.setMsgReceiver(UID);
                message.setArtID(-1);
                message.setMsgContent("非常抱歉，您的贴文：'" + articles.get(0).getArtTitle() + "'未通过审核");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();//获取当前时间
                message.setMsgTime(formatter.format(date));
                message.setMsgFromUser(0);
                message.setMsgIsReaded(0);
            }
            if (messageService.insertMessage(message) != 1) return "databaseFalse";//发送消息
        } else {//审核不通过贴文失败
            msg = "notPassArticleFalse";
        }

        System.out.println("贴文不通过审核: " + msg);
        return msg;
    }

    /**
     * 删除贴文
     * 响应"/deleteArticle.do"请求
     * @param ArtID 贴文ID
     * @return msg 返回前端校验信息
     * 删除步骤:
     *   1.贴文文字置'该贴已被删除'，贴文图片均置null
     *   2.向用户ID发送信息
     */
    @ResponseBody
    @RequestMapping("/manage/deleteArticle.do")
    public String deleteArticle(int ArtID) {
        String msg = null;
        Message message = new Message();

        //在删除前获得贴文标题
        List<Article> articles = articleService.selectArticleByArtID(ArtID);//找到贴文
        if (articles.size() == 0) return "databaseFalse";
        String ArtTitle = articles.get(0).getArtTitle();

        if (articleService.updateArticleByArtIDDelete(ArtID) == 1) {//贴文删除成功
            msg = "deleteArticleSuccess";

            //发送消息
            message.setMsgReceiver(articles.get(0).getUID());
            message.setArtID(-1);
            message.setMsgContent("非常抱歉，您的贴文：'" + ArtTitle + "'已被管理员删除");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();//获取当前时间
            message.setMsgTime(formatter.format(date));
            message.setMsgFromUser(0);
            message.setMsgIsReaded(0);
            if (messageService.insertMessage(message) != 1) return "databaseFalse";//发送消息
        } else {//贴文删除失败
            msg = "deleteArticleFalse";
        }

        System.out.println("删除贴文: " + msg);
        return msg;
    }

    /**
     * 刷新页面
     * 响应"/refreshArticle.do"请求
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/refreshArticle.do")
    public List<Article> refreshArticle() {
        //禁言操作后需要存储新的完整查询结果
        List<Article> articles = null;

        if (ifShowAllArticle == 1) articles = articleService.selectAllArticle();
        else articles = articleService.selectAllBeCheckedArticle();

        selectArticles = articles;//存储完整查询结果
        articlePaging.recordCount = selectArticles.size();
        articlePaging.pageCount = selectArticles.size() % articlePaging.maxRecordNum == 0 ? selectArticles.size() / articlePaging.maxRecordNum : selectArticles.size() / articlePaging.maxRecordNum + 1;

        return articlePaging.showPageContent(selectArticles);//调用分页显示方法
    }

    /**
     * 展示贴文，不展示审核不通过的贴文
     * 响应"/showArticle.do"请求
     * @param ArtID 贴文ID
     * @return List<Article> 返回贴文集合
     */
    @ResponseBody
    @RequestMapping("/manage/showArticle.do")
    public List<Article> showArticle(int ArtID) {
        List<Article> articles = articleService.selectArticleByArtID(ArtID);//按贴文ID查找贴文记录

        for (int i = 0; i < articles.size(); i++) {
            if (articles.get(i).getArtStatus() == -1) {
                articles.remove(i);
                i = i - 1;
            }
        }

        System.out.println("展示贴文: size:" + articles.size());
        return articles;
    }

    /********************************************客户端请求********************************************/

    /**
     * 插入贴文
     * 响应"/insertArticle.do"请求
     * @param UID 用户ID
     * @param ArtCategory 贴文类别
     * @param ArtTitle 贴文标题
     * @param ArtContent 贴文文字
     * @param ArtPic1 贴文图片，base64编码
     * @param ArtPic2 贴文图片
     * @param ArtPic3 贴文图片
     * @param ArtPic4 贴文图片
     * @param ArtPic5 贴文图片
     * @param ArtPic6 贴文图片
     * @return msg 返回前端校验信息
     * 备注:
     *   若因MySQL长度限制而返回'贴文插入失败'，
     *   1.在MySQL的my.ini文件的[mysqlid]下加入max_allowed_packet=50M
     *   2.或使用临时命令:set global max_allowed_packet = 52428800
     *   3.重新启动mysql服务
     */
    @ResponseBody
    @RequestMapping("/insertArticle.do")
    public String insertArticle(int UID, String ArtCategory, String ArtTitle, String ArtContent, String ArtPic1, String ArtPic2, String ArtPic3, String ArtPic4, String ArtPic5, String ArtPic6) {
        if (userService.selectUserByUID(UID).get(0).getUbanLevel() == -1) return "userBaning";

        String msg = null;
        Article article = new Article();

        /*String[] ArtPic = new String[6];
        ArtPic[0] = ArtPic1;
        ArtPic[1] = ArtPic2;
        ArtPic[2] = ArtPic3;
        ArtPic[3] = ArtPic4;
        ArtPic[4] = ArtPic5;
        ArtPic[5] = ArtPic6;*/

        String ArtPic[] = {ArtPic1, ArtPic2, ArtPic3, ArtPic4, ArtPic5, ArtPic6};

        //处理图片数据
        for (int i = 0; i < 6; i++) {
            if (ArtPic[i] == null) continue;
            if (ArtPic[i].equals("null")) continue;

            byte[] decodePic;
            try {
                byte[] encryptedBytes = ArtPic[i].getBytes("UTF-8");
                //byte[] decryptedBytes = Base64.decodeBase64(encryptedBytes);//java.util.Base64方法
                byte[] decryptedBytes = Base64.decodeBase64(encryptedBytes);//apache.commons.codec.binary.Base64方法
                decodePic = decryptedBytes;
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }

            switch (i) {
                case 0:
                    article.setArtPic1(decodePic);
                    break;
                case 1:
                    article.setArtPic2(decodePic);
                    break;
                case 2:
                    article.setArtPic3(decodePic);
                    break;
                case 3:
                    article.setArtPic4(decodePic);
                    break;
                case 4:
                    article.setArtPic5(decodePic);
                    break;
                case 5:
                    article.setArtPic6(decodePic);
                    break;
            }
        }

        //添加其他属性
        article.setUID(UID);
        article.setArtCategory(ArtCategory);
        article.setArtTitle(ArtTitle);
        article.setArtContent(ArtContent);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();//获取当前时间
        article.setArtSubTime(formatter.format(date));

        if (articleService.insertArticle(article) == 1) msg = "insertArticleSuccess";//贴文插入成功
        else msg = "insertArticleFalse";//贴文插入失败

        System.out.println("插入贴文: " + msg);
        return msg;
    }

    /*用于分页功能*/
    private List<ArticleList> selectArticleLists = null;//存储完整查询结果
    private ArticleListPaging articleListPaging = new ArticleListPaging();//分页类

    /**
     * 查询所有个人发布贴文列表
     * 响应"/searchAllPerSubArtList.do"请求
     * @param UID 用户ID
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/searchAllPerSubArtList.do")
    public List<ArticleList> searchAllPerSubArtList(int UID) {
        List<Article> articles = articleService.selectArticleByUID(UID);//按用户ID查找贴文记录

        List<ArticleList> articleLists = new ArrayList<>();
        ArticleList articleList = null;
        for (Article article: articles) {
            articleList = new ArticleList();
            articleList.setArtID(article.getArtID());
            articleList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
            articleList.setArtCategory(article.getArtCategory());
            if (article.getArtStatus() == 0) articleList.setArtTitle("贴文待审核");
            else articleList.setArtTitle(article.getArtTitle());
            articleList.setArtFavorNum(article.getArtFavorNum());
            articleList.setArtCollNum(article.getArtCollNum());
            articleList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());
            articleList.setArtSubTime(article.getArtSubTime());
            articleList.setArtStatus(article.getArtStatus());

            articleLists.add(articleList);
        }

        selectArticleLists = articleLists;//存储完整查询结果
        articleListPaging.recordCount = selectArticleLists.size();
        articleListPaging.pageCount = selectArticleLists.size() % articleListPaging.maxRecordNum == 0 ? selectArticleLists.size() / articleListPaging.maxRecordNum : selectArticleLists.size() / articleListPaging.maxRecordNum + 1;
        articleListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有个人发布贴文列表: size:" + articleLists.size());
        return articleListPaging.showPageContent(selectArticleLists);//调用分页显示方法
    }

    /**
     * 查询个人发布贴文列表
     * 响应"/searchPerSubArtList.do"请求
     * @param UID 用户ID
     * @param searchContent 搜索内容
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/searchPerSubArtList.do")
    public List<ArticleList> searchPerSubArtList(int UID, String searchContent) {
        List<Article> articles = articleService.selectArticleByUID(UID);//按用户ID查找贴文记录

        List<ArticleList> articleLists = new ArrayList<>();
        ArticleList articleList = null;
        for (Article article: articles) {
            if (article.getArtTitle().contains(searchContent)) {
                articleList = new ArticleList();
                articleList.setArtID(article.getArtID());
                articleList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
                articleList.setArtCategory(article.getArtCategory());
                if (article.getArtStatus() == 0) articleList.setArtTitle("贴文待审核");
                else articleList.setArtTitle(article.getArtTitle());
                articleList.setArtFavorNum(article.getArtFavorNum());
                articleList.setArtCollNum(article.getArtCollNum());
                articleList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());
                articleList.setArtSubTime(article.getArtSubTime());
                articleList.setArtStatus(article.getArtStatus());

                articleLists.add(articleList);
            }
        }

        selectArticleLists = articleLists;//存储完整查询结果
        articleListPaging.recordCount = selectArticleLists.size();
        articleListPaging.pageCount = selectArticleLists.size() % articleListPaging.maxRecordNum == 0 ? selectArticleLists.size() / articleListPaging.maxRecordNum : selectArticleLists.size() / articleListPaging.maxRecordNum + 1;
        articleListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询个人发布贴文列表: size:" + articleLists.size());
        return articleListPaging.showPageContent(selectArticleLists);//调用分页显示方法
    }

    /**
     * 访客空间查询所有个人发布贴文列表
     * 响应"/searchAllPerSubArtListVisitor.do"请求
     * @param UID 用户ID
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/searchAllPerSubArtListVisitor.do")
    public List<ArticleList> searchAllPerSubArtListVisitor(int UID) {
        List<Article> articles = articleService.selectArticleByUID(UID);//按用户ID查找贴文记录

        List<ArticleList> articleLists = new ArrayList<>();
        ArticleList articleList = null;
        for (Article article: articles) {
            if (article.getArtStatus() == 1) {//只查询审核通过贴文
                articleList = new ArticleList();
                articleList.setArtID(article.getArtID());
                articleList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
                articleList.setArtCategory(article.getArtCategory());
                if (article.getArtStatus() == 0) articleList.setArtTitle("贴文待审核");
                else articleList.setArtTitle(article.getArtTitle());
                articleList.setArtFavorNum(article.getArtFavorNum());
                articleList.setArtCollNum(article.getArtCollNum());
                articleList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());
                articleList.setArtSubTime(article.getArtSubTime());
                articleList.setArtStatus(article.getArtStatus());

                articleLists.add(articleList);
            }
        }

        selectArticleLists = articleLists;//存储完整查询结果
        articleListPaging.recordCount = selectArticleLists.size();
        articleListPaging.pageCount = selectArticleLists.size() % articleListPaging.maxRecordNum == 0 ? selectArticleLists.size() / articleListPaging.maxRecordNum : selectArticleLists.size() / articleListPaging.maxRecordNum + 1;
        articleListPaging.currentPage = 0;//当前页码为0

        System.out.println("访客空间查询所有个人发布贴文列表: size:" + articleLists.size());
        return articleListPaging.showPageContent(selectArticleLists);//调用分页显示方法
    }

    /**
     * 分页下一页
     * 响应"/nextArticleListPage.do"请求
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/nextArticleListPage.do")
    public List<ArticleList> nextArticleListPage() {
        if (!articleListPaging.isLastPage(selectArticleLists)) {//当前页不是最后一页
            articleListPaging.currentPage++;//当前页码+1
        } else return new ArrayList<>();
        return articleListPaging.showPageContent(selectArticleLists);//调用分页显示方法
    }

    /**
     * 分页最前页
     * 响应"/topArticleListPage.do"请求
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/topArticleListPage.do")
    public List<ArticleList> topArticleListPage() {
        articleListPaging.currentPage = 0;//当前页码为0
        return articleListPaging.showPageContent(selectArticleLists);//调用分页显示方法
    }

    /*用于分页功能*/
    private List<ExploreList> selectExploreLists = null;//存储完整查询结果
    private ExploreListPaging exploreListPaging = new ExploreListPaging();//分页类

    /**
     * 查询所有浏览贴文列表
     * 响应"/searchAllExploreList.do"请求
     * @param ArtCategory 贴文类别
     * @return List<ExploreList> 返回浏览列表集合
     */
    @ResponseBody
    @RequestMapping("/searchAllExploreList.do")
    public List<ExploreList> searchAllExploreList(String ArtCategory) {
        List<Article> articles = articleService.selectArticleByArtCategoryOrderByArtSubTime(ArtCategory);//按贴文类别查找贴文记录，并按贴文发布时间降序

        List<ExploreList> exploreLists = new ArrayList<>();
        ExploreList exploreList = null;
        for (Article article: articles) {
            if (article.getArtStatus() == 1) {//只显示已审核通过的贴文
                exploreList = new ExploreList();
                exploreList.setArtID(article.getArtID());
                exploreList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
                exploreList.setArtSubTime(article.getArtSubTime());
                exploreList.setArtCategory(article.getArtCategory());
                exploreList.setArtTitle(article.getArtTitle());
                exploreList.setArtContent(article.getArtContent());
                exploreList.setArtPic1(article.getArtPic1());
                exploreList.setArtFavorNum(article.getArtFavorNum());
                exploreList.setArtCollNum(article.getArtCollNum());
                exploreList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());

                exploreLists.add(exploreList);
            }
        }

        selectExploreLists = exploreLists;//存储完整查询结果
        exploreListPaging.recordCount = selectExploreLists.size();
        exploreListPaging.pageCount = selectExploreLists.size() % exploreListPaging.maxRecordNum == 0 ? selectExploreLists.size() / exploreListPaging.maxRecordNum : selectExploreLists.size() / exploreListPaging.maxRecordNum + 1;
        exploreListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有浏览贴文列表: size:" + exploreLists.size());
        return exploreListPaging.showPageContent(selectExploreLists);//调用分页显示方法
    }

    /**
     * 查询浏览贴文列表
     * 响应"/searchExploreList.do"请求
     * @param ArtCategory 贴文类别
     * @param searchContent 搜索内容
     * @return List<ExploreList> 返回浏览列表集合
     */
    @ResponseBody
    @RequestMapping("/searchExploreList.do")
    public List<ExploreList> searchExploreList(String ArtCategory, String searchContent) {
        List<Article> articles = articleService.selectArticleByArtCategoryOrderByArtSubTime(ArtCategory);//按贴文类别查找贴文记录，并按贴文发布时间降序

        List<ExploreList> exploreLists = new ArrayList<>();
        ExploreList exploreList = null;
        for (Article article: articles) {
            if (article.getArtStatus() == 1) {//只显示已审核通过的贴文
                if (article.getArtTitle().contains(searchContent)) {
                    exploreList = new ExploreList();
                    exploreList.setArtID(article.getArtID());
                    exploreList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
                    exploreList.setArtSubTime(article.getArtSubTime());
                    exploreList.setArtCategory(article.getArtCategory());
                    exploreList.setArtTitle(article.getArtTitle());
                    exploreList.setArtContent(article.getArtContent());
                    exploreList.setArtPic1(article.getArtPic1());
                    exploreList.setArtFavorNum(article.getArtFavorNum());
                    exploreList.setArtCollNum(article.getArtCollNum());
                    exploreList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());

                    exploreLists.add(exploreList);
                }
            }
        }

        selectExploreLists = exploreLists;//存储完整查询结果
        exploreListPaging.recordCount = selectExploreLists.size();
        exploreListPaging.pageCount = selectExploreLists.size() % exploreListPaging.maxRecordNum == 0 ? selectExploreLists.size() / exploreListPaging.maxRecordNum : selectExploreLists.size() / exploreListPaging.maxRecordNum + 1;
        exploreListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询浏览贴文列表: size:" + exploreLists.size());
        return exploreListPaging.showPageContent(selectExploreLists);//调用分页显示方法
    }

    /**
     * 分页下一页
     * 响应"/nextExploreListPage.do"请求
     * @return List<ExploreList> 返回浏览列表集合
     */
    @ResponseBody
    @RequestMapping("/nextExploreListPage.do")
    public List<ExploreList> nextExploreListPage() {
        if (!exploreListPaging.isLastPage(selectExploreLists)) {//当前页不是最后一页
            exploreListPaging.currentPage++;//当前页码+1
        } else return new ArrayList<>();
        return exploreListPaging.showPageContent(selectExploreLists);//调用分页显示方法
    }

    /**
     * 分页最前页
     * 响应"/topExploreListPage.do"请求
     * @return List<ExploreList> 返回浏览列表集合
     */
    @ResponseBody
    @RequestMapping("/topExploreListPage.do")
    public List<ExploreList> topExploreListPage() {
        exploreListPaging.currentPage = 0;//当前页码为0
        return exploreListPaging.showPageContent(selectExploreLists);//调用分页显示方法
    }

    /**
     * 显示贴文详情
     * 响应"/showArticleDetail.do"请求
     * @param ArtID 贴文ID
     * @param UID 用户ID
     * @return List<ArticleDetail> 返回贴文详情集合
     */
    @ResponseBody
    @RequestMapping("/showArticleDetail.do")
    public List<ArticleDetail> showArticleDetail(int ArtID, int UID) {
        List<Article> articles = articleService.selectArticleByArtID(ArtID);//按贴文ID查找贴文记录

        List<ArticleDetail> articleDetails = new ArrayList<>();
        ArticleDetail articleDetail = new ArticleDetail();

        articleDetail.setUname(userService.selectUserByUID(articles.get(0).getUID()).get(0).getUname());
        articleDetail.setArtCategory(articles.get(0).getArtCategory());
        articleDetail.setArtTitle(articles.get(0).getArtTitle());
        articleDetail.setArtContent(articles.get(0).getArtContent());
        articleDetail.setArtPic1(articles.get(0).getArtPic1());
        articleDetail.setArtPic2(articles.get(0).getArtPic2());
        articleDetail.setArtPic3(articles.get(0).getArtPic3());
        articleDetail.setArtPic4(articles.get(0).getArtPic4());
        articleDetail.setArtPic5(articles.get(0).getArtPic5());
        articleDetail.setArtPic6(articles.get(0).getArtPic6());
        articleDetail.setArtSubTime(articles.get(0).getArtSubTime());
        articleDetail.setArtFavorNum(articles.get(0).getArtFavorNum());
        articleDetail.setArtCollNum(articles.get(0).getArtCollNum());
        articleDetail.setIsFavored(favorService.selectFavorByUIDArtID(UID, ArtID).size());
        articleDetail.setIsCollected(collectionService.selectCollectionByUIDArtID(UID, ArtID).size());

        articleDetails.add(articleDetail);

        System.out.println("显示贴文详情: size:" + articleDetails.size());
        return articleDetails;
    }
}
