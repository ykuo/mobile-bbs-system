package com.tust.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/*控制层Controller页面跳转类*/
@Controller
public class JumpController {
    /********************************************管理端请求********************************************/

    /**
     * 跳转到相应页面
     * 响应"/goToJSP.do"请求
     * @Param("Aname") Aname
     * @Param("modelName") modelName
     * @return ModelAndView类型页面
     */
    @RequestMapping("/manage/goToJSP.do")
    public ModelAndView goToJSP(String Aname, String modelName) {
        ModelAndView mv = new ModelAndView();
        if (modelName.equals("管理主页")) {
            mv.addObject("Aname", Aname);
            mv.addObject("modelName", modelName);
            mv.setViewName("/manage/MainManage.jsp");
        } else if (modelName.equals("用户管理")) {
            mv.addObject("Aname", Aname);
            mv.addObject("modelName", modelName);
            mv.setViewName("/manage/UserManage.jsp");
        } else if (modelName.equals("贴文管理")) {
            mv.addObject("Aname", Aname);
            mv.addObject("modelName", modelName);
            mv.setViewName("/manage/ArticleManage.jsp");
        } else if (modelName.equals("评论管理")) {
            mv.addObject("Aname", Aname);
            mv.addObject("modelName", modelName);
            mv.setViewName("/manage/CommentManage.jsp");
        }

        System.out.println("跳转到相应页面: " + modelName);
        return mv;
    }

    /**
     * 跳转到贴文详情页面
     * 响应"/goToArticleDetail.do"请求
     * @param ArtID 贴文ID
     * @return ModelAndView类型页面
     */
    @RequestMapping("/manage/goToArticleDetail.do")
    public ModelAndView goToArticleDetail(int ArtID) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("ArtID", ArtID);
        mv.setViewName("/manage/ArticleDetail.jsp");

        System.out.println("跳转到贴文详情页面: ArtID：" + ArtID);
        return mv;
    }
}
