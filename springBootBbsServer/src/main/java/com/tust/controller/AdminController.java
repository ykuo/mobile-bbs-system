package com.tust.controller;

import com.tust.domain.Admin;
import com.tust.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/*控制层Controller管理员类*/
@Controller
public class AdminController {
    /*自动注入Service管理员接口*/
    @Autowired
    private AdminService adminService;

    /********************************************管理端请求********************************************/

    /**
     * 管理员登录
     * 响应"/adminLogin.do"请求
     * @param Aname 管理员名
     * @param Apasswd 管理员密码
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/manage/adminLogin.do")
    public String adminLogin(String Aname, String Apasswd) {
        List<Admin> admins = adminService.selectAdminByAname(Aname);//按管理员名查找管理员记录
        String msg = null;

        if (admins.size() == 1) {//管理员存在
            msg = "passwordFalse";//密码错误
            if (admins.get(0).getApasswd().equals(Apasswd)) msg = "loginSuccess";//登录成功
        } else {//管理员不存在
            msg = "adminNotExist";
        }

        System.out.println("管理员登录: " + msg);
        return msg;
    }
}
