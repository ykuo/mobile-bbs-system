package com.tust.controller;

import com.tust.domain.Message;
import com.tust.paging.MessageListPaging;
import com.tust.service.MessageService;
import com.tust.service.UserService;
import com.tust.trans.MessageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/*控制层Controller消息类*/
@Controller
public class MessageController {
    /*自动注入Service消息接口*/
    @Autowired
    private MessageService messageService;
    /*自动注入Service用户接口*/
    @Autowired
    private UserService userService;

    /********************************************客户端请求********************************************/

    /*用于分页功能*/
    private List<MessageList> selectMessageLists = null;//存储完整查询结果
    private MessageListPaging messageListPaging = new MessageListPaging();//分页类

    /**
     * 查询所有管理员消息列表
     * 响应"/searchAllAdminMsgList.do"请求
     * @param MsgReceiver 接收方
     * @return List<MessageList> 返回消息列表集合
     */
    @ResponseBody
    @RequestMapping("/searchAllAdminMsgList.do")
    public List<MessageList> searchAllAdminMsgList(int MsgReceiver) {
        List<Message> messages = messageService.selectMessageByMsgReceiver(MsgReceiver);//按接收方查找消息记录

        List<MessageList> messageLists = new ArrayList<>();
        MessageList messageList = null;
        for (Message message: messages) {
            if (message.getMsgFromUser() == 0) {//消息来自管理员
                messageList = new MessageList();
                messageList.setMsgID(message.getMsgID());
                messageList.setMsgSenderName("管理员");
                messageList.setMsgContent(message.getMsgContent());
                messageList.setArtID(message.getArtID());
                messageList.setMsgTime(message.getMsgTime());
                messageList.setMsgIsReaded(message.getMsgIsReaded());

                messageLists.add(messageList);
            }
        }

        selectMessageLists = messageLists;//存储完整查询结果
        messageListPaging.recordCount = selectMessageLists.size();
        messageListPaging.pageCount = selectMessageLists.size() % messageListPaging.maxRecordNum == 0 ? selectMessageLists.size() / messageListPaging.maxRecordNum : selectMessageLists.size() / messageListPaging.maxRecordNum + 1;
        messageListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有管理员消息列表: size:" + messageLists.size());
        return messageListPaging.showPageContent(selectMessageLists);//调用分页显示方法
    }

    /**
     * 查询所有用户消息列表
     * 响应"/searchAllUserMsgList.do"请求
     * @param MsgReceiver 接收方
     * @return List<MessageList> 返回消息列表集合
     */
    @ResponseBody
    @RequestMapping("/searchAllUserMsgList.do")
    public List<MessageList> searchAllUserMsgList(int MsgReceiver) {
        List<Message> messages = messageService.selectMessageByMsgReceiver(MsgReceiver);//按接收方查找消息记录

        List<MessageList> messageLists = new ArrayList<>();
        MessageList messageList = null;
        for (Message message: messages) {
            if (message.getMsgFromUser() == 1) {//消息来自用户
                messageList = new MessageList();
                messageList.setMsgID(message.getMsgID());
                messageList.setMsgSenderName(userService.selectUserByUID(message.getMsgSender()).get(0).getUname());
                messageList.setMsgContent(message.getMsgContent());
                messageList.setArtID(message.getArtID());
                messageList.setMsgTime(message.getMsgTime());
                messageList.setMsgIsReaded(message.getMsgIsReaded());

                messageLists.add(messageList);
            }
        }

        selectMessageLists = messageLists;//存储完整查询结果
        messageListPaging.recordCount = selectMessageLists.size();
        messageListPaging.pageCount = selectMessageLists.size() % messageListPaging.maxRecordNum == 0 ? selectMessageLists.size() / messageListPaging.maxRecordNum : selectMessageLists.size() / messageListPaging.maxRecordNum + 1;
        messageListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有管理员消息列表: size:" + messageLists.size());
        return messageListPaging.showPageContent(selectMessageLists);//调用分页显示方法
    }

    /**
     * 阅读消息
     * 响应"/readMsg.do"请求
     * @param MsgID 消息ID
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/readMsg.do")
    public String readMsg(int MsgID) {
        String msg = null;

        if (messageService.updateMessageByMsgIDRead(MsgID, 1) == 1) {//按消息ID修改消息是否已读
            msg = "readMsgSuccess";//阅读消息成功
        } else msg = "readMsgFalse";//阅读消息失败

        System.out.println("阅读消息: " + msg);
        return msg;
    }

    /**
     * 分页下一页
     * 响应"/nextMessageListPage.do"请求
     * @return List<MessageList> 返回消息列表集合
     */
    @ResponseBody
    @RequestMapping("/nextMessageListPage.do")
    public List<MessageList> nextMessageListPage() {
        if (!messageListPaging.isLastPage(selectMessageLists)) {//当前页不是最后一页
            messageListPaging.currentPage++;//当前页码+1
        } else return new ArrayList<>();
        return messageListPaging.showPageContent(selectMessageLists);//调用分页显示方法
    }

    /**
     * 分页最前页
     * 响应"/topMessageListPage.do"请求
     * @return List<MessageList> 返回消息列表集合
     */
    @ResponseBody
    @RequestMapping("/topMessageListPage.do")
    public List<MessageList> topMessageListPage() {
        messageListPaging.currentPage = 0;//当前页码为0
        return messageListPaging.showPageContent(selectMessageLists);//调用分页显示方法
    }
}
