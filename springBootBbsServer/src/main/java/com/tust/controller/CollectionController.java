package com.tust.controller;

import com.tust.domain.*;
import com.tust.paging.ArticleListPaging;
import com.tust.service.*;
import com.tust.trans.ArticleList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*控制层Controller收藏类*/
@Controller
public class CollectionController {
    /*自动注入Service收藏接口*/
    @Autowired
    private CollectionService collectionService;
    /*自动注入Service消息接口*/
    @Autowired
    private MessageService messageService;
    /*自动注入Service贴文接口*/
    @Autowired
    private ArticleService articleService;
    /*自动注入Service用户接口*/
    @Autowired
    private UserService userService;
    /*自动注入Service评论接口*/
    @Autowired
    private CommentService commentService;

    /********************************************客户端请求********************************************/

    /*用于分页功能*/
    private List<ArticleList> selectCollArtLists = null;//存储完整查询结果
    private ArticleListPaging collArtListPaging = new ArticleListPaging();//分页类

    /**
     * 查询所有收藏贴文列表
     * 响应"/searchAllCollArtList.do"请求
     * @param UID 用户ID
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/searchAllCollArtList.do")
    public List<ArticleList> searchAllCollArtList(int UID) {
        List<Collection> collections = collectionService.selectCollectionByUID(UID);//按用户ID查找收藏记录

        List<ArticleList> articleLists = new ArrayList<>();
        Article article = null;
        ArticleList articleList = null;
        for (Collection collection: collections) {//按贴文ID查找贴文记录
            article = articleService.selectArticleByArtID(collection.getArtID()).get(0);

            articleList = new ArticleList();
            articleList.setArtID(article.getArtID());
            articleList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
            articleList.setArtCategory(article.getArtCategory());
            articleList.setArtTitle(article.getArtTitle());
            articleList.setArtFavorNum(article.getArtFavorNum());
            articleList.setArtCollNum(article.getArtCollNum());
            articleList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());
            articleList.setArtSubTime(article.getArtSubTime());
            articleList.setArtStatus(-2);

            articleLists.add(articleList);
        }

        selectCollArtLists = articleLists;//存储完整查询结果
        collArtListPaging.recordCount = selectCollArtLists.size();
        collArtListPaging.pageCount = selectCollArtLists.size() % collArtListPaging.maxRecordNum == 0 ? selectCollArtLists.size() / collArtListPaging.maxRecordNum : selectCollArtLists.size() / collArtListPaging.maxRecordNum + 1;
        collArtListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询所有收藏贴文列表: size:" + articleLists.size());
        return collArtListPaging.showPageContent(selectCollArtLists);//调用分页显示方法
    }

    /**
     * 查询收藏贴文列表
     * 响应"/searchCollArtList.do"请求
     * @param UID 用户ID
     * @param searchContent 搜索内容
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/searchCollArtList.do")
    public List<ArticleList> searchCollArtList(int UID, String searchContent) {
        List<Collection> collections = collectionService.selectCollectionByUID(UID);//按用户ID查找收藏记录

        List<ArticleList> articleLists = new ArrayList<>();
        Article article = null;
        ArticleList articleList = null;
        for (Collection collection: collections) {//按贴文ID查找贴文记录
            article = articleService.selectArticleByArtID(collection.getArtID()).get(0);

            if (article.getArtTitle().contains(searchContent)) {
                articleList = new ArticleList();
                articleList.setArtID(article.getArtID());
                articleList.setUname(userService.selectUserByUID(article.getUID()).get(0).getUname());
                articleList.setArtCategory(article.getArtCategory());
                articleList.setArtTitle(article.getArtTitle());
                articleList.setArtFavorNum(article.getArtFavorNum());
                articleList.setArtCollNum(article.getArtCollNum());
                articleList.setArtComNum(commentService.selectPassCommentByArtID(article.getArtID()).size());
                articleList.setArtSubTime(article.getArtSubTime());
                articleList.setArtStatus(-2);

                articleLists.add(articleList);
            }
        }

        selectCollArtLists = articleLists;//存储完整查询结果
        collArtListPaging.recordCount = selectCollArtLists.size();
        collArtListPaging.pageCount = selectCollArtLists.size() % collArtListPaging.maxRecordNum == 0 ? selectCollArtLists.size() / collArtListPaging.maxRecordNum : selectCollArtLists.size() / collArtListPaging.maxRecordNum + 1;
        collArtListPaging.currentPage = 0;//当前页码为0

        System.out.println("查询收藏贴文列表: size:" + articleLists.size());
        return collArtListPaging.showPageContent(selectCollArtLists);//调用分页显示方法
    }

    /**
     * 分页下一页
     * 响应"/nextCollArtListPage.do"请求
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/nextCollArtListPage.do")
    public List<ArticleList> nextCollArtListPage() {
        if (!collArtListPaging.isLastPage(selectCollArtLists)) {//当前页不是最后一页
            collArtListPaging.currentPage++;//当前页码+1
        } else return new ArrayList<>();
        return collArtListPaging.showPageContent(selectCollArtLists);//调用分页显示方法
    }

    /**
     * 分页最前页
     * 响应"/topCollArtListPage.do"请求
     * @return List<ArticleList> 返回贴文列表集合
     */
    @ResponseBody
    @RequestMapping("/topCollArtListPage.do")
    public List<ArticleList> topCollArtListPage() {
        collArtListPaging.currentPage = 0;//当前页码为0
        return collArtListPaging.showPageContent(selectCollArtLists);//调用分页显示方法
    }

    /**
     * 收藏贴文
     * 响应"/collectArticle.do"请求
     * @param ArtID 贴文ID
     * @param UID 用户ID
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/collectArticle.do")
    public String collectArticle(int ArtID, int UID) {
        String msg = null;
        Message message = new Message();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();//获取当前时间
        Collection collection = new Collection(UID, ArtID, formatter.format(date));

        if (collectionService.insertCollection(collection) == 1) {//插入收藏成功
            msg ="insertCollectionSuccess";

            //发送消息
            List<Article> articles = articleService.selectArticleByArtID(ArtID);//找到贴文
            List<User> users = userService.selectUserByUID(UID);//找到用户
            if (articles.size() == 0 || users.size() == 0) return "databaseFalse";
            message.setMsgReceiver(articles.get(0).getUID());
            message.setArtID(ArtID);
            message.setMsgContent("您的贴文：'" + articles.get(0).getArtTitle() + "'被" + users.get(0).getUname() + "收藏");
            date = new Date();//获取当前时间
            message.setMsgTime(formatter.format(date));
            message.setMsgFromUser(0);
            message.setMsgIsReaded(0);
            if (messageService.insertMessage(message) != 1) return "databaseFalse";//发送消息
        } else msg = "insertCollectionFalse";//插入收藏失败

        System.out.println("收藏贴文：" + msg);
        return msg;
    }

    /**
     * 取消收藏贴文
     * 响应"/cancelCollectArticle.do"请求
     * @param ArtID 贴文ID
     * @param UID 用户ID
     * @return msg 返回前端校验信息
     */
    @ResponseBody
    @RequestMapping("/cancelCollectArticle.do")
    public String cancelCollectArticle(int ArtID, int UID) {
        String msg = null;

        if (collectionService.deleteCollection(UID, ArtID) == 1) msg ="deleteCollectionSuccess";//删除收藏成功
        else msg = "deleteCollectionFalse";//删除收藏失败

        System.out.println("取消收藏：" + msg);
        return msg;
    }
}
