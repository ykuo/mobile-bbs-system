package com.tust.paging;

import com.tust.trans.CommentList;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * 分页类 lombok自动生成构造方法
 * 成员：maxRecordNum 每页最大记录数量
 * 成员：recordCount 总记录条数
 * 成员：pageCount 分页数量
 * 成员：currentPage 当前页码
 * 方法：showPageContent 分页显示方法
 * 方法：isLastPage 判断当前页是否是最后一页
 * 方法：isFirstPage 判断当前页是否是第一页
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CommentListPaging implements Paging{
    public int maxRecordNum = 10;//每页最大10条记录
    public int recordCount;//记录条数
    public int pageCount;//分页数量
    public int currentPage;//当前页码

    /**
     * 分页显示方法
     * @param selectCommentLists 查询结果
     * @return
     */
    public List<CommentList> showPageContent(List<CommentList> selectCommentLists) {
        List<CommentList> showRecordLists;
        int beginRecord = currentPage * maxRecordNum;//起始显示记录
        int endRecord = 0;//最终显示记录

        if (recordCount > maxRecordNum) {//需要分页时
            if (recordCount <= (currentPage + 1) * maxRecordNum) {//当前页是最后一页
                endRecord = recordCount;
            } else {//当前页不是最后一页
                endRecord = (currentPage + 1) * maxRecordNum;
            }
            showRecordLists = selectCommentLists.subList(beginRecord, endRecord);
        } else showRecordLists = selectCommentLists;//不需要分页时

        return showRecordLists;
    }

    /**
     * 判断当前页是否是最后一页
     * @param selectCommentLists 查询结果
     * @return
     */
    public boolean isLastPage(List<CommentList> selectCommentLists) {
        if (selectCommentLists.size() <= (currentPage + 1) * maxRecordNum) return true;//当前页是最后一页
        else return false;//当前页不是最后一页
    }

    /**
     * 判断当前页是否是第一页
     * @return
     */
    public boolean isFirstPage() {
        if (currentPage == 0) return true;//当前页是第一页
        else return false;//当前页不是第一页
    }
}
