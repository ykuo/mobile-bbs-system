package com.tust.paging;

/**
 * 分页接口
 * 成员：maxRecordNum 每页最大记录数量
 * 成员：recordCount 总记录条数
 * 成员：pageCount 分页数量
 * 成员：currentPage 当前页码
 */
public interface Paging {
    int maxRecordNum = 10;//每页最大10条记录
    int recordCount = 0;//记录条数
    int pageCount = 0;//分页数量
    int currentPage = 0;//当前页码
}
