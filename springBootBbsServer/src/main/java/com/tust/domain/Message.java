package com.tust.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 实体类 lombok自动生成构造方法
 * 表名：消息
 * 属性：消息ID 实型 自动增长 主键
 * 属性：发送方 实型
 * 属性：接收方 实型 非空 外键
 * 属性：贴文ID 实型 非空 规定-1来自管理员
 * 属性：消息内容 变长字符600 非空
 * 属性：消息发送时间 日期时间型 非空
 * 属性：是否来自用户 小实型 非空 自定义约束（0or1） 规定默认0来自管理员，1来自用户
 * 属性：是否已读 小实型 非空 自定义约束（0or1） 规定默认0未读，1已读
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private int MsgID;
    private int MsgSender;
    private int MsgReceiver;
    private int ArtID;
    private String MsgContent;
    private String MsgTime;
    private int MsgFromUser;
    private int MsgIsReaded;
}
