package com.tust.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 实体类 lombok自动生成构造方法
 * 表名：管理员
 * 属性：管理员ID 实型 自动增长 主键
 * 属性：管理员名 变长字符20 非空 唯一
 * 属性：管理员密码 变长字符10 非空
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Admin {
    private int AID;
    private String Aname;
    private String Apasswd;
}
