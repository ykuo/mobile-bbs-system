package com.tust.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 实体类 lombok自动生成构造方法
 * 表名：收藏
 * 属性：用户ID 实型 主键 外键
 * 属性：贴文ID 实型 主键 外键
 * 属性：收藏时间 日期时间型 非空
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Collection {
    private int UID;
    private int ArtID;
    private String CollTime;
}
