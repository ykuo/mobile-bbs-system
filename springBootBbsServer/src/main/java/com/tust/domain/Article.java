package com.tust.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 实体类 lombok自动生成构造方法
 * 表名：贴文
 * 属性：贴文ID 实型 自动增长 主键
 * 属性：用户ID 实型 非空 外键
 * 属性：贴文类别 变长字符20 非空
 * 属性：贴文标题 变长字符200 非空
 * 属性：贴文文字 变长字符2000 非空
 * 属性：贴文图片1 长二进制
 * 属性：贴文图片2 长二进制
 * 属性：贴文图片3 长二进制
 * 属性：贴文图片4 长二进制
 * 属性：贴文图片5 长二进制
 * 属性：贴文图片6 长二进制
 * 属性：贴文发布时间 日期时间型 非空
 * 属性：贴文点赞量 实型 非空
 * 属性：贴文收藏量 实型 非空
 * 属性：贴文审核状态 小实型 非空 自定义约束（0or1or-1） 规定默认0待审核，1通过，-1不通过
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Article {
    private int ArtID;
    private int UID;
    private String ArtCategory;
    private String ArtTitle;
    private String ArtContent;
    private byte[] ArtPic1;
    private byte[] ArtPic2;
    private byte[] ArtPic3;
    private byte[] ArtPic4;
    private byte[] ArtPic5;
    private byte[] ArtPic6;
    private String ArtSubTime;
    private int ArtFavorNum;
    private int ArtCollNum;
    private int ArtStatus;
}
