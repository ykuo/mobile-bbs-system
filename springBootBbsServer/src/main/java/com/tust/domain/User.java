package com.tust.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 实体类 lombok自动生成构造方法
 * 表名：用户
 * 属性：用户ID 实型 自动增长 主键
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：用户密码 变长字符10 非空
 * 属性：用户性别 字符2 自定义约束（男or女）
 * 属性：用户简介 变长字符600
 * 属性：用户点赞隐私 小实型 非空 自定义约束（0or1） 规定默认0显示，1不显示
 * 属性：用户收藏隐私 小实型 非空 自定义约束（0or1） 规定默认0显示，1不显示
 * 属性：用户禁言等级 小实型 非空 规定默认0不禁言，审核不通过+1，若>=3禁言1天，并置-1禁言
 * 属性：用户禁言到期时间 日期时间型 默认null
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private int UID;
    private String Uname;
    private String Upasswd;
    private String Ugender;
    private String Uint;
    private int UfavorPrivacy;
    private int UcollPrivacy;
    private int UbanLevel;
    private String UbanTime;
}
