package com.tust.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 实体类 lombok自动生成构造方法
 * 表名：评论
 * 属性：评论ID 实型 自动增长 主键
 * 属性：用户ID 实型 非空 外键
 * 属性：贴文ID 实型 非空 外键
 * 属性：评论内容 变长字符600 非空
 * 属性：评论时间 日期时间型 非空
 * 属性：评论审核状态 小实型 非空 自定义约束（0or1or-1） 规定默认0待审核，1通过，-1不通过
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    private int ComID;
    private int UID;
    private int ArtID;
    private String ComContent;
    private String ComTime;
    private int ComStatus;
}
