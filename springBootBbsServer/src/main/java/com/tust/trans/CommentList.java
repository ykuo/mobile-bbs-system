package com.tust.trans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 通信类 lombok自动生成构造方法
 * 类名：评论列表
 * 属性：贴文ID 实型 非空 外键
 * 属性：贴文类别 变长字符20 非空
 * 属性：贴文标题 变长字符200 非空
 * 属性：评论内容 变长字符600 非空
 * 属性：评论时间 日期时间型 非空
 * 属性：评论审核状态 小实型 非空 自定义约束（0or1or-1） 规定默认0待审核，1通过，-1不通过
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CommentList {
    private int ArtID;
    private String ArtCategory;
    private String ArtTitle;
    private String ComContent;
    private String ComTime;
    private int ComStatus;
}
