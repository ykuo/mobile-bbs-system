package com.tust.trans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 通信类 lombok自动生成构造方法
 * 类名：消息列表
 * 属性：消息ID 实型 自动增长 主键
 * 属性：发送方姓名 变长字符20 用户名或管理员名
 * 属性：消息内容 变长字符600 非空
 * 属性：贴文ID 实型 非空 外键
 * 属性：消息发送时间 日期时间型 非空
 * 属性：是否已读 小实型 非空 自定义约束（0or1） 规定默认0未读，1已读
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MessageList {
    private int MsgID;
    private String MsgSenderName;
    private String MsgContent;
    private int ArtID;
    private String MsgTime;
    private int MsgIsReaded;
}
