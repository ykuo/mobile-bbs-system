package com.tust.trans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 通信类 lombok自动生成构造方法
 * 类名：贴文详情
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：贴文类别 变长字符20 非空
 * 属性：贴文标题 变长字符200 非空
 * 属性：贴文文字 变长字符2000 非空
 * 属性：贴文图片1 长二进制
 * 属性：贴文图片2 长二进制
 * 属性：贴文图片3 长二进制
 * 属性：贴文图片4 长二进制
 * 属性：贴文图片5 长二进制
 * 属性：贴文图片6 长二进制
 * 属性：贴文发布时间 日期时间型 非空
 * 属性：贴文点赞量 实型 非空
 * 属性：贴文收藏量 实型 非空
 * 属性：是否已点赞 实型 非数据库 规定0未点赞，1已点赞
 * 属性：是否已收藏 实型 非数据库 规定0未收藏，1已收藏
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ArticleDetail {
    private String Uname;
    private String ArtCategory;
    private String ArtTitle;
    private String ArtContent;
    private byte[] ArtPic1;
    private byte[] ArtPic2;
    private byte[] ArtPic3;
    private byte[] ArtPic4;
    private byte[] ArtPic5;
    private byte[] ArtPic6;
    private String ArtSubTime;
    private int ArtFavorNum;
    private int ArtCollNum;
    private int isFavored;
    private int isCollected;
}
