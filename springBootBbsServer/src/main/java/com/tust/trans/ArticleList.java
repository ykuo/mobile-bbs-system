package com.tust.trans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 通信类 lombok自动生成构造方法
 * 类名：贴文列表
 * 属性：贴文ID 实型 自动增长 主键
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：贴文类别 变长字符20 非空
 * 属性：贴文标题 变长字符200 非空
 * 属性：贴文点赞量 实型 非空
 * 属性：贴文收藏量 实型 非空
 * 属性：贴文评论量 实型 非数据库
 * 属性：贴文发布时间 日期时间型 非空
 * 属性：贴文审核状态 小实型 非空 自定义约束（0or1or-1） 规定默认0待审核，1通过，-1不通过
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ArticleList {
    private int ArtID;
    private String Uname;
    private String ArtCategory;
    private String ArtTitle;
    private int ArtFavorNum;
    private int ArtCollNum;
    private int ArtComNum;
    private String ArtSubTime;
    private int ArtStatus;
}
