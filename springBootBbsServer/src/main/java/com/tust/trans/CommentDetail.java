package com.tust.trans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 通信类 lombok自动生成构造方法
 * 类名：评论详情
 * 属性：用户ID 实型 非空 外键
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：评论内容 变长字符600 非空
 * 属性：评论时间 日期时间型 非空
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CommentDetail {
    private int UID;
    private String Uname;
    private String ComContent;
    private String ComTime;
}
