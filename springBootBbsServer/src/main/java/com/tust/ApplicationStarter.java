package com.tust;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetAddress;
import java.net.UnknownHostException;

/*SpringBoot启动类，增加快捷启动链接*/
@MapperScan("com.tust.dao")
@SpringBootApplication
@Slf4j
public class ApplicationStarter {
    public static void main(String[] args) throws UnknownHostException {
        SpringApplication.run(ApplicationStarter.class, args);
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = "8080";
        String path = "/manage/AdminLogin.jsp";
        log.info(
                "\n\n" +
                "┌—─—————————————————————————————————————————————————————————————————————————————————┐\n" +
                "│\t\t\t Application is running! Access URLs:\t\t\t\t\t\t\t\t\t│\n" +
                "│\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t│\n" +
                "│\t\t\t 贴文相关测试网址: \thttp://localhost:" + port + "/testSubmitArticle.jsp" + "\t\t\t│\n" +
                "│\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t│\n" +
                "│\t\t\t Local访问网址: \t\thttp://localhost:" + port + path + "\t\t\t\t│\n" +
                "│\t\t\t External访问网址: \thttp://" + ip + ":" + port + path + "\t\t\t│\n" +
                "└———————————————————————————————————————————————————————————————————————————————————┘\n\n"
        );
    }
}
