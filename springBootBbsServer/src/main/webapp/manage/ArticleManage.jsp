<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--http://localhost:8080/manage/ArticleManage.jsp -->

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>ArticleManage</title>
        <link rel="stylesheet" href="../css/Manage.css">
        <script src="../js/jquery-3.1.1.min.js"></script>
    </head>

    <body>
        <!--头部-->
        <div id="div_head">
            <!--当前模块名称-->
            <span id="span_modelName" style="display: none">${modelName}</span>

            <!--后台管理系统名称-->
            <span id="span_listIcon"></span>
            <span id="span_bbsSysManage">BBS系统管理后台</span>

            <!--管理员名称-->
            <span id="span_admin">管理员：</span>
            <span id="span_Aname">${Aname}</span>
            <button id="btn_logout" type="button"></button>
        </div>

        <!--导航-->
        <div id="div_navi">
            <!--管理主页页面跳转按钮-->
            <form id="form_goToMainManage" action="goToJSP.do" method="post">
                <span id="span_homeIcon"></span>
                <input type="hidden" name="modelName" value="管理主页"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToMainManage" type="button">管理主页</button>
            </form>

            <!--用户管理页面跳转按钮-->
            <form id="form_goToUserManage" action="goToJSP.do" method="post">
                <span id="span_groupIcon"></span>
                <input type="hidden" name="modelName" value="用户管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToUserManage" type="button">用户管理</button>
            </form>

            <!--贴文管理页面跳转按钮-->
            <form id="form_goToArticleManage" action="goToJSP.do" method="post" style="background: white; opacity: 62%;">
                <span id="span_libraryBooksIcon"></span>
                <input type="hidden" name="modelName" value="贴文管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToArticleManage" type="button" style="color: rgba(32, 160, 255, 1);">贴文管理</button>
            </form>

            <!--评论管理页面跳转按钮-->
            <form id="form_goToCommentManage" action="goToJSP.do" method="post">
                <span id="span_forumIcon"></span>
                <input type="hidden" name="modelName" value="评论管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToCommentManage" type="button">评论管理</button>
            </form>
        </div>

        <!--主体-->
        <div id="div_mainPart">
            <!--查询栏-->
            <div id="div_select">
                <span>选择查询项：</span>
                <!--下拉菜单-->
                <select id="select_selectOption">
                    <option style="display: none" selected>下拉选择</option>
                    <option>贴文ID</option>
                    <option>用户ID</option>
                    <option>用户名</option>
                    <option>贴文类别</option>
                    <option>贴文标题</option>
                    <option>贴文文字</option>
                    <option>审核状态</option>
                    <option>发布时间</option>
                </select>&nbsp&nbsp&nbsp&nbsp
                <input id="input_selectContent" type="text" placeholder="输入查询内容"/>&nbsp&nbsp&nbsp&nbsp
                <button id="bt_selectArticle" type="button">查询</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <button id="bt_selectAllBeCheckedArticle" type="button">显示待审核贴文</button>
                <button id="bt_selectAllArticle" type="button" style="display: none">显示所有贴文</button>
            </div>

            <!--数据栏-->
            <div id="div_data">
                <!--数据以表格形式展示-->
                <table>
                    <thead>
                    <tr>
                        <th>贴文ID</th>
                        <th>发布时间</th>
                        <th>用户ID</th>
                        <th>贴文类别</th>
                        <th>贴文标题</th>
                        <th>审核状态</th>
                        <th>审核</th>
                        <th>删除</th>
                    </tr>
                    </thead>
                    <tbody id="tbody_data"></tbody>
                </table>

                <!--分页按钮-->
                </br><div id="div_paging"/>
            </div>
        </div>
    </body>

    <script>
        //保存当前数据表显示状态(默认显示所有数据)
        var ifShowAllArticle = 1;

        //ready方法
        $(document).ready(function () {
            selectAjax("selectAllArticle.do");
        });

        //后台管理系统图标点击事件
        $("#span_listIcon").click(function () {
            $("#form_goToMainManage").submit();
        });

        //管理主页按钮点击事件
        $("#btn_goToMainManage").click(function() {
            $("#form_goToMainManage").submit();
        });

        //用户管理按钮点击事件
        $("#btn_goToUserManage").click(function() {
            $("#form_goToUserManage").submit();
        });

        //贴文管理按钮点击事件
        $("#btn_goToArticleManage").click(function() {
            $("#form_goToArticleManage").submit();
        });

        //评论管理按钮点击事件
        $("#btn_goToCommentManage").click(function() {
            $("#form_goToCommentManage").submit();
        });

        //注销管理员按钮点击事件
        $("#btn_logout").click(function() {
            alert("注销成功！");
            location.href="AdminLogin.jsp";
        });

        //查询按钮点击事件
        $("#bt_selectArticle").click(function () {
            var selectOption = $("#select_selectOption option:selected").val();
            var selectContent = $("#input_selectContent").val();
            var reg = /^(\d{4})-(\d{2})-(\d{2})$/;

            if (selectOption == "下拉选择") alert("请选择查询项！");
            else if (selectContent == "") alert("请输入查询内容！");
            else if ((selectOption == "贴文ID" || selectOption == "用户ID" || selectOption == "审核状态") && isNaN(selectContent)) alert("请输入数字！");
            else if (selectOption == "审核状态" && (parseInt(selectContent) > 1 || parseInt(selectContent) < -1)) alert("请输入-1~1的数字！");
            else if (selectOption == "发布时间" && !reg.test(selectContent)) alert("请输入yyyy-mm-dd日期格式！");
            else {
                $.ajax({
                    url:"selectArticle.do",
                    data: {//请求数据
                        "selectOption":selectOption,
                        "selectContent":selectContent
                    },
                    type:"post",//请求类型
                    dataType:"json",//响应数据返回格式
                    success:function(data) {//返回成功处理函数
                        console.log(data);
                        //alert(selectOption+"  "+selectContent+"  "+data);
                        if (data.length == 0) alert("查询结果为空！");
                        else {
                            alert("查询成功！");
                            $("#tbody_data").html("");//清空数据表
                            var artStatus;
                            $.each(data, function (index, article) {
                                if (article.artStatus == 0) artStatus = "待审核";
                                else if (article.artStatus == -1) artStatus = "不通过";
                                else if (article.artStatus == 1) artStatus = "通过";
                                $("#tbody_data").append(
                                    '<tr>' +
                                    '<td><a href="/manage/goToArticleDetail.do?ArtID=' + article.artID + '" target="_black">' + article.artID + '</a></td>' +
                                    '<td>' + article.artSubTime + '</td>' +
                                    '<td>' + article.uid + '</td>' +
                                    '<td>' + article.artCategory + '</td>' +
                                    '<td>' + article.artTitle + '</td>' +
                                    '<td>' + artStatus + '</td>' +
                                    '<td><a href="javascript:void(0);" onclick=passArticle(' + article.artID + ',' + article.artStatus + ')>通过</a>&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=notPassArticle(' + article.artID + ',' + article.artStatus + ')>不通过</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick=deleteArticle(' + article.artID + ')>删除</a></td>' +
                                    '</tr>'
                                );
                            })
                            $.ajax({
                                url: "getArticlePageInfo.do",
                                data: {//请求数据
                                    "tableStatus": ifShowAllArticle
                                },
                                type: "post",//请求类型
                                dataType: "json",//响应数据返回格式
                                success: function (data) {//返回成功处理函数
                                    console.log(data);
                                    //alert(data);
                                    var currentPage = data.currentPage + 1;
                                    $("#div_paging").html("");//清空分页按钮
                                    $("#div_paging").append(
                                        '<a>共' + data.recordCount + '条，分' + data.pageCount + '页，当前第' + currentPage + '页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=topPage()>最前页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=prePage()>上一页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=nextPage()>下一页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=endPage()>最尾页</a>'
                                    );
                                },
                                error: function () {//返回失败处理函数
                                    alert("ajax返回失败！");
                                }
                            })
                        }
                    },
                    error:function() {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            }
        });

        //显示待审核贴文按钮点击事件
        $("#bt_selectAllBeCheckedArticle").click(function () {
            ifShowAllArticle = 0;
            $("#bt_selectAllArticle").css('display','inline');
            $("#bt_selectAllBeCheckedArticle").css('display','none');
            selectAjax("selectAllBeCheckedArticle.do");
        });

        //显示所有贴文按钮点击事件
        $("#bt_selectAllArticle").click(function () {
            ifShowAllArticle = 1;
            $("#bt_selectAllBeCheckedArticle").css('display','inline');
            $("#bt_selectAllArticle").css('display','none');
            selectAjax("selectAllArticle.do");
        });

        //上一页点击方法
        function prePage() {
            selectAjax("preArticlePage.do");
        };

        //下一页点击方法
        function nextPage() {
            selectAjax("nextArticlePage.do");
        };

        //最前页点击方法
        function topPage() {
            selectAjax("topArticlePage.do");
        };

        //最尾页点击方法
        function endPage() {
            selectAjax("endArticlePage.do");
        };

        //无参查询请求(用于页面刷新和分页)+内嵌分页信息请求
        function selectAjax(selectURL) {
            $.ajax({
                url:selectURL,
                type:"post",//请求类型
                dataType:"json",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    //console.log(data);
                    $("#tbody_data").html("");//清空数据表
                    if (data.length == 0) alert("没有记录！");
                    else {
                        //alert("查询成功！");
                        var artStatus;
                        $.each(data, function (index, article) {
                            if (article.artStatus == 0) artStatus = "待审核";
                            else if (article.artStatus == -1) artStatus = "不通过";
                            else if (article.artStatus == 1) artStatus = "通过";
                            $("#tbody_data").append(
                                '<tr>' +
                                '<td><a href="/manage/goToArticleDetail.do?ArtID=' + article.artID + '" target="_black">' + article.artID + '</a></td>' +
                                '<td>' + article.artSubTime + '</td>' +
                                '<td>' + article.uid + '</td>' +
                                '<td>' + article.artCategory + '</td>' +
                                '<td>' + article.artTitle + '</td>' +
                                '<td>' + artStatus + '</td>' +
                                '<td><a href="javascript:void(0);" onclick=passArticle(' + article.artID + ',' + article.artStatus + ')>通过</a>&nbsp&nbsp' +
                                '<a href="javascript:void(0);" onclick=notPassArticle(' + article.artID + ',' + article.artStatus + ')>不通过</a></td>' +
                                '<td><a href="javascript:void(0);" onclick=deleteArticle(' + article.artID + ')>删除</a></td>' +
                                '</tr>'
                            );
                        })
                        $.ajax({
                            url: "getArticlePageInfo.do",
                            data: {//请求数据
                                "tableStatus": ifShowAllArticle
                            },
                            type: "post",//请求类型
                            dataType: "json",//响应数据返回格式
                            success: function (data) {//返回成功处理函数
                                console.log(data);
                                //alert(data);
                                var currentPage = data.currentPage + 1;
                                $("#div_paging").html("");//清空分页按钮
                                $("#div_paging").append(
                                    '<a>共' + data.recordCount + '条，分' + data.pageCount + '页，当前第' + currentPage + '页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=topPage()>最前页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=prePage()>上一页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=nextPage()>下一页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=endPage()>最尾页</a>'
                                );
                            },
                            error: function () {//返回失败处理函数
                                alert("ajax返回失败！");
                            }
                        })
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        };

        //审核通过贴文方法
        function passArticle(ArtID, ArtStatus) {
            if (ArtStatus == '0') {
                $.ajax({
                    url: "passArticle.do",//请求路径
                    data: {//请求数据
                        "ArtID": ArtID
                    },
                    type: "post",//请求类型
                    dataType: "text",//响应数据返回格式
                    success: function (data) {//返回成功处理函数
                        console.log(data);
                        if (data == "databaseFalse") alert("读写数据库失败！");
                        else if (data == "passArticleFalse") alert("审核通过贴文失败！");
                        else if (data == "passArticleSuccess") {
                            alert("审核通过贴文成功！");
                            selectAjax("refreshArticle.do");
                        }
                    },
                    error: function () {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            } else alert("已经审核操作！");
        };

        //审核不通过贴文方法
        function notPassArticle(ArtID, ArtStatus) {
            if (ArtStatus == '0') {
                $.ajax({
                    url:"notPassArticle.do",//请求路径
                    data: {//请求数据
                        "ArtID":ArtID
                    },
                    type:"post",//请求类型
                    dataType:"text",//响应数据返回格式
                    success:function(data) {//返回成功处理函数
                        console.log(data);
                        if (data == "databaseFalse") alert("读写数据库失败！");
                        else if (data == "notPassArticleFalse") alert("审核不通过贴文失败！");
                        else if (data == "notPassArticleSuccess") {
                            alert("审核不通过贴文成功！");
                            selectAjax("refreshArticle.do");
                        }
                    },
                    error:function() {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            } else alert("已经审核操作！");
        };

        //删除贴文方法
        function deleteArticle(ArtID) {
            $.ajax({
                url:"deleteArticle.do",//请求路径
                data: {//请求数据
                    "ArtID":ArtID
                },
                type:"post",//请求类型
                dataType:"text",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    console.log(data);
                    if (data == "databaseFalse") alert("读写数据库失败！");
                    else if (data == "deleteArticleFalse") alert("删除贴文失败！");
                    else if (data == "deleteArticleSuccess") {
                        alert("删除贴文成功！");
                        selectAjax("refreshArticle.do");
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        };
    </script>
</html>
