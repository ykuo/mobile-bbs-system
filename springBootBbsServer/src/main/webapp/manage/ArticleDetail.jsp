<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--http://localhost:8080/manage/ArticleDetail.jsp -->

<html>
<head>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>ArticleDetail</title>
        <link rel="stylesheet" href="../css/ArticleDetail.css">
        <script src="../js/jquery-3.1.1.min.js"></script>
    </head>

    <body>
        <h2 id="h2_title">ArticleDetail</h2>

        <div id="div_ArticleDetail">
            <!--贴文展示区域-->
            <div id="div_article">
                <p id="p_ArtTitle"/>
                <p id="p_ArtCategory"/>
                <p id="p_Uname"/>
                <p id="p_ArtSubTime"/>
                <p id="p_ArtContent"/>
                <div id="div_img"></div>
            </div>

            <hr/><!--分割线-->

            <!--评论展示区域-->
            <div id="div_comment"></div><br/>
        </div>
    </body>

    <script>
        var ArtStatus = 1;

        //ready方法
        $(document).ready(function () {
            $.ajax({
                url:"showArticle.do",//请求路径
                data: {//请求数据
                    "ArtID":${ArtID}
                },
                type:"post",//请求类型
                dataType:"json",//响应数据返回格式
                async:false,//同步请求，保证执行顺序
                success:function(data) {//返回成功处理函数
                    console.log(data);
                    //alert(data);
                    if (data.length == 0) {
                        alert("该贴文经审核：审核不通过！");
                        ArtStatus = 0;
                    } else {
                        //alert("查询成功！");

                        //查询用户名
                        var Uname;
                        $.ajax({
                            url: "selectUname.do",//请求路径
                            data: {//请求数据
                                "UID": data[0].uid
                            },
                            type: "post",//请求类型
                            dataType: "json",//响应数据返回格式
                            async: false,//同步请求，保证执行顺序
                            success: function (userData) {//返回成功处理函数
                                console.log(userData);
                                //alert(userData);
                                if (data.length == 0) alert("查询用户结果为空！");
                                else {
                                    //alert("查询用户成功！");
                                    Uname = userData[0].uname;
                                }
                            },
                            error: function () {//返回失败处理函数
                                alert("ajax返回失败！");
                            }
                        })

                        $("#p_ArtTitle").text(data[0].artTitle);
                        $("#p_ArtCategory").text("类别：" + data[0].artCategory);
                        $("#p_Uname").text("用户名：" + Uname);
                        $("#p_ArtSubTime").text("发布时间：" + data[0].artSubTime);
                        $("#p_ArtContent").text(data[0].artContent);

                        var src;//存储图片数据
                        for (let i = 0; i < 6; i++) {
                            //存储图片数据
                            switch (i) {
                                case 0:
                                    src = data[0].artPic1;
                                    break;
                                case 1:
                                    src = data[0].artPic2;
                                    break;
                                case 2:
                                    src = data[0].artPic3;
                                    break;
                                case 3:
                                    src = data[0].artPic4;
                                    break;
                                case 4:
                                    src = data[0].artPic5;
                                    break;
                                case 5:
                                    src = data[0].artPic6;
                                    break;
                            }

                            if (src != null) {
                                //处理图片数据
                                src = src.replaceAll('dataimage/jpegbase64', '');//网页端上传，去除头部字符串
                                src = src.replaceAll('=', '');//尾部去除'='冗余
                                src = 'data:image/jpeg;base64,' + src;//安卓端上传，增加头部字符串

                                $("#div_img").append(
                                    /*'<a>' + src + '<a/></br>' +*/
                                    '<img src="' + src + '"/>&nbsp'
                                );
                            } else break;
                        }
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })

            if (ArtStatus == 1) {
                $.ajax({
                    url:"showComment.do",
                    data: {//请求数据
                        "ArtID":${ArtID}
                    },
                    type:"post",//请求类型
                    dataType:"json",//响应数据返回格式
                    async:false,//同步请求，保证执行顺序
                    success:function(data) {//返回成功处理函数
                        console.log(data);
                        //alert(data);
                        if (data.length == 0) {
                            //alert("查询评论结果为空！");
                            $("#div_comment").append(
                                '<br/><span>没有评论</span>'
                            );
                        } else {
                            //alert("查询成功！");

                            $.each(data, function (index, comment) {
                                //查询用户名
                                var Uname;
                                $.ajax({
                                    url: "selectUname.do",//请求路径
                                    data: {//请求数据
                                        "UID": comment.uid
                                    },
                                    type: "post",//请求类型
                                    dataType: "json",//响应数据返回格式
                                    async: false,//同步请求，保证执行顺序
                                    success: function (userData) {//返回成功处理函数
                                        console.log(userData);
                                        //alert(userData);
                                        if (data.length == 0) alert("查询用户结果为空！");
                                        else {
                                            //alert("查询用户成功！");
                                            Uname = userData[0].uname;
                                        }
                                    },
                                    error: function () {//返回失败处理函数
                                        alert("ajax返回失败！");
                                    }
                                })

                                $("#div_comment").prepend(
                                    '<span>审核状态：' + comment.comStatus + '</span>&nbsp&nbsp&nbsp'+
                                    '<span>' + comment.comTime + '</span>&nbsp&nbsp&nbsp' +
                                    '<span>' + Uname + '：</span>&nbsp' +
                                    '<span>' + comment.comContent + '</span><br/>'
                                );
                            })

                            $("#div_comment").prepend(
                                '<p>评论区：</p><br/>'
                            );
                        }
                    },
                    error:function() {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            }
        });
    </script>
</html>
