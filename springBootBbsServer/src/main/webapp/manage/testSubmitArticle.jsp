<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--http://localhost:8080/manage/testSubmitArticle.jsp -->

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>testSubmitArticle</title>
        <%--<link rel="stylesheet" href="css/testSubmitArticle.css">--%>
        <script src="../js/jquery-3.1.1.min.js"></script>
    </head>

    <body>
        <h2>testSubmitArticle</h2>

        <!--储存图片数据-->
        <img id="img0" style="display: none">
        <img id="img1" style="display: none">
        <img id="img2" style="display: none">
        <img id="img3" style="display: none">
        <img id="img4" style="display: none">
        <img id="img5" style="display: none">

        <!--贴文发布测试-->
        <form>
            <a>贴文发布测试</a>
            <p>&nbsp用户&nbspID&nbsp：<input id="input_UID" type="text" value="1"/></p>
            <p>贴文类别：<input id="input_ArtCategory" type="text" value="数码"/></p>
            <p>贴文标题：<input id="input_ArtTitle" type="text" value="笔记本"/></p>
            <p>贴文内容：<input id="input_ArtContent" type="text" value="展示一下市面上的笔记本:"/></p>
            <p>贴文图片：<input id="input_ArtPic" type="file" accept="image/jpeg" multiple/></p>
            <button id="bt_testArticleSubmit" type="button">提交测试贴文</button>
        </form>

        <br/><br/>

        <!--贴文展示测试-->
        <form>
            <a>贴文展示测试</a>
            <p>&nbsp贴文&nbspID&nbsp：<input id="input_ArtID" type="text" value="1"></p>
            <button id="bt_testArticleShow" type="button">显示测试贴文</button>
        </form>

        <!--贴文展示区域-->
        <div id="div_article"/>
    </body>

    <script>
        //贴文图片上传点击事件
        $("#input_ArtPic").click(function() {
            event.target.value = null;//清空input标签的files数组
        });

        //选择文件change事件
        $("#input_ArtPic").change(function () {
            for (let i = 0; i < 6; i++) $("#img"+i).attr("src", null);//清空储存图片数据

            var fileNum = $("#input_ArtPic").prop('files').length;
            if (fileNum > 6) {
                alert("最大读取6张图片！");
                event.target.value = null;
            } else {
                for (let i = 0; i < fileNum; i++) {
                    let reader = new FileReader();
                    reader.readAsDataURL($("#input_ArtPic").get(0).files[i]);
                    reader.onloadend = function () {
                        switch (i) {
                            case 0:
                                $("#img0").attr("src", reader.result);
                                break;
                            case 1:
                                $("#img1").attr("src", reader.result);
                                break;
                            case 2:
                                $("#img2").attr("src", reader.result);
                                break;
                            case 3:
                                $("#img3").attr("src", reader.result);
                                break;
                            case 4:
                                $("#img4").attr("src", reader.result);
                                break;
                            case 5:
                                $("#img5").attr("src", reader.result);
                                break;
                        }
                    }
                }
            }
        });

        //提交按钮点击事件
        $("#bt_testArticleSubmit").click(function() {
            var UID = $("#input_UID").val();//id选择器
            var ArtCategory = $("#input_ArtCategory").val();
            var ArtTitle = $("#input_ArtTitle").val();
            var ArtContent = $("#input_ArtContent").val();
            var ArtPic1 = $("#img0").attr("src");
            var ArtPic2 = $("#img1").attr("src");
            var ArtPic3 = $("#img2").attr("src");
            var ArtPic4 = $("#img3").attr("src");
            var ArtPic5 = $("#img4").attr("src");
            var ArtPic6 = $("#img5").attr("src");

            $.ajax({
                url:"insertArticle.do",//请求路径
                data: {//请求数据
                    "UID":UID,
                    "ArtCategory":ArtCategory,
                    "ArtTitle":ArtTitle,
                    "ArtContent":ArtContent,
                    "ArtPic1":ArtPic1,
                    "ArtPic2":ArtPic2,
                    "ArtPic3":ArtPic3,
                    "ArtPic4":ArtPic4,
                    "ArtPic5":ArtPic5,
                    "ArtPic6":ArtPic6
                },
                type:"post",//请求类型
                dataType:"text",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    console.log(data);
                    //alert(data);
                    if (data == "ArtPicFalse") alert("图片处理失败！");
                    else if (data == "insertArticleSuccess") alert("贴文插入成功！");
                    else if (data == "insertArticleFalse") alert("贴文插入失败！");
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        });

        //显示按钮点击事件
        $("#bt_testArticleShow").click(function() {
            var ArtID = $("#input_ArtID").val();

            $.ajax({
                url:"showArticle.do",//请求路径
                data: {//请求数据
                    "ArtID":ArtID
                },
                type:"post",//请求类型
                dataType:"json",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    console.log(data);
                    //alert(data);
                    if (data.length == 0) alert("查询结果为空！");
                    else {
                        alert("查询成功！");

                        $("#div_article").html("");//清空贴文展示区域
                        $("#div_article").append(
                            '<a>ArtTitle:' + data[0].artTitle + '</a>&nbsp&nbsp' +
                            '<a>ArtCategory:' + data[0].artCategory + '</a><br/>' +
                            '<a>UID:' + data[0].uid + '</a>&nbsp&nbsp' +
                            '<a>ArtSubTime:' + data[0].artSubTime + '</a><br/><br/>' +
                            '<a>ArtContent:' + data[0].artContent + '</a><br/><br/>'
                        );

                        var src;//存储图片数据
                        var imgFomate;//存储图片格式
                        for (let i = 0; i < 6; i++) {
                            //存储图片数据
                            switch (i) {
                                case 0:
                                    src = data[0].artPic1;
                                    break;
                                case 1:
                                    src = data[0].artPic2;
                                    break;
                                case 2:
                                    src = data[0].artPic3;
                                    break;
                                case 3:
                                    src = data[0].artPic4;
                                    break;
                                case 4:
                                    src = data[0].artPic5;
                                    break;
                                case 5:
                                    src = data[0].artPic6;
                                    break;
                            }

                            if (src != null) {
                                /*$("#div_article").append(
                                    '<a>源' + src + '</a></br>'
                                );*/
                                //处理图片数据
                                src = src.replaceAll('dataimage/jpegbase64', '');//网页端上传，去除头部字符串
                                src = src.replaceAll('=', '');//尾部去除'='冗余
                                src = 'data:image/jpeg;base64,' + src;//安卓端上传，增加头部字符串

                                $("#div_article").append(
                                    '<a>' + src + '</a></br>' +
                                    '<img src="' + src + '"/>'
                                );
                            } else break;
                        }
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        });
    </script>
</html>
