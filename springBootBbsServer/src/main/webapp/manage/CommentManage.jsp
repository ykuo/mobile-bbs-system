<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--http://localhost:8080/manage/CommentManage.jsp -->

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>CommentManage</title>
        <link rel="stylesheet" href="../css/Manage.css">
        <script src="../js/jquery-3.1.1.min.js"></script>
    </head>

    <body>
        <!--头部-->
        <div id="div_head">
            <!--当前模块名称-->
            <span id="span_modelName" style="display: none">${modelName}</span>

            <!--后台管理系统名称-->
            <span id="span_listIcon"></span>
            <span id="span_bbsSysManage">BBS系统管理后台</span>

            <!--管理员名称-->
            <span id="span_admin">管理员：</span>
            <span id="span_Aname">${Aname}</span>
            <button id="btn_logout" type="button"></button>
        </div>

        <!--导航-->
        <div id="div_navi">
            <!--管理主页页面跳转按钮-->
            <form id="form_goToMainManage" action="goToJSP.do" method="post">
                <span id="span_homeIcon"></span>
                <input type="hidden" name="modelName" value="管理主页"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToMainManage" type="button">管理主页</button>
            </form>

            <!--用户管理页面跳转按钮-->
            <form id="form_goToUserManage" action="goToJSP.do" method="post">
                <span id="span_groupIcon"></span>
                <input type="hidden" name="modelName" value="用户管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToUserManage" type="button">用户管理</button>
            </form>

            <!--贴文管理页面跳转按钮-->
            <form id="form_goToArticleManage" action="goToJSP.do" method="post">
                <span id="span_libraryBooksIcon"></span>
                <input type="hidden" name="modelName" value="贴文管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToArticleManage" type="button">贴文管理</button>
            </form>

            <!--评论管理页面跳转按钮-->
            <form id="form_goToCommentManage" action="goToJSP.do" method="post" style="background: white; opacity: 62%;">
                <span id="span_forumIcon"></span>
                <input type="hidden" name="modelName" value="评论管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToCommentManage" type="button" style="color: rgba(32, 160, 255, 1);">评论管理</button>
            </form>
        </div>

        <!--主体-->
        <div id="div_mainPart">
            <!--查询栏-->
            <div id="div_select">
                <span>选择查询项：</span>
                <!--下拉菜单-->
                <select id="select_selectOption">
                    <option style="display: none" selected>下拉选择</option>
                    <option>评论ID</option>
                    <option>用户ID</option>
                    <option>贴文ID</option>
                    <option>用户名</option>
                    <option>评论内容</option>
                    <option>审核状态</option>
                    <option>评论时间</option>
                </select>&nbsp&nbsp&nbsp&nbsp
                <input id="input_selectContent" type="text" placeholder="输入查询内容"/>&nbsp&nbsp&nbsp&nbsp
                <button id="bt_selectComment" type="button">查询</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <button id="bt_selectAllBeCheckedComment" type="button">显示待审核评论</button>
                <button id="bt_selectAllComment" type="button" style="display: none">显示所有评论</button>
            </div>

            <!--数据栏-->
            <div id="div_data">
                <!--数据以表格形式展示-->
                <table>
                    <thead>
                    <tr>
                        <th>评论ID</th>
                        <th>评论时间</th>
                        <th>用户ID</th>
                        <th>贴文ID</th>
                        <th>评论内容</th>
                        <th>审核状态</th>
                        <th>审核</th>
                        <th>删除</th>
                    </tr>
                    </thead>
                    <tbody id="tbody_data"></tbody>
                </table>

                <!--分页按钮-->
                </br><div id="div_paging"/>
            </div>
        </div>
    </body>

    <script>
        //保存当前数据表显示状态(默认显示所有数据)
        var ifShowAllComment = 1;

        //ready方法
        $(document).ready(function () {
            selectAjax("selectAllComment.do");
        });

        //后台管理系统图标点击事件
        $("#span_listIcon").click(function () {
            $("#form_goToMainManage").submit();
        });

        //管理主页按钮点击事件
        $("#btn_goToMainManage").click(function() {
            $("#form_goToMainManage").submit();
        });

        //用户管理按钮点击事件
        $("#btn_goToUserManage").click(function() {
            $("#form_goToUserManage").submit();
        });

        //贴文管理按钮点击事件
        $("#btn_goToArticleManage").click(function() {
            $("#form_goToArticleManage").submit();
        });

        //评论管理按钮点击事件
        $("#btn_goToCommentManage").click(function() {
            $("#form_goToCommentManage").submit();
        });

        //注销管理员按钮点击事件
        $("#btn_logout").click(function() {
            alert("注销成功！");
            location.href="AdminLogin.jsp";
        });

        //查询按钮点击事件
        $("#bt_selectComment").click(function () {
            var selectOption = $("#select_selectOption option:selected").val();
            var selectContent = $("#input_selectContent").val();
            var reg = /^(\d{4})-(\d{2})-(\d{2})$/;

            if (selectOption == "下拉选择") alert("请选择查询项！");
            else if (selectContent == "") alert("请输入查询内容！");
            else if ((selectOption == "评论ID" || selectOption == "用户ID" || selectOption == "贴文ID" || selectOption == "审核状态") && isNaN(selectContent)) alert("请输入数字！");
            else if (selectOption == "审核状态" && (parseInt(selectContent) > 1 || parseInt(selectContent) < -1)) alert("请输入-1~1的数字！");
            else if (selectOption == "评论时间" && !reg.test(selectContent)) alert("请输入yyyy-mm-dd日期格式！");
            else {
                $.ajax({
                    url:"selectComment.do",
                    data: {//请求数据
                        "selectOption":selectOption,
                        "selectContent":selectContent
                    },
                    type:"post",//请求类型
                    dataType:"json",//响应数据返回格式
                    success:function(data) {//返回成功处理函数
                        console.log(data);
                        //alert(selectOption+"  "+selectContent+"  "+data);
                        if (data.length == 0) alert("查询结果为空！");
                        else {
                            alert("查询成功！");
                            $("#tbody_data").html("");//清空数据表
                            var comStatus;
                            $.each(data, function (index, comment) {
                                if (comment.comStatus == 0) comStatus = "待审核";
                                else if (comment.comStatus == -1) comStatus = "不通过";
                                else if (comment.comStatus == 1) comStatus = "通过";
                                $("#tbody_data").append(
                                    '<tr>' +
                                    '<td>' + comment.comID + '</td>' +
                                    '<td>' + comment.comTime + '</td>' +
                                    '<td>' + comment.uid + '</td>' +
                                    '<td><a href="/manage/goToArticleDetail.do?ArtID=' + comment.artID + '" target="_black">' + comment.artID + '</a></td>' +
                                    '<td>' + comment.comContent + '</td>' +
                                    '<td>' + comStatus + '</td>' +
                                    '<td><a href="javascript:void(0);" onclick=passComment(' + comment.comID + ',' + comment.comStatus + ')>通过</a>&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=notPassComment(' + comment.comID + ',' + comment.comStatus + ')>不通过</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick=deleteComment(' + comment.comID + ')>删除</a></td>' +
                                    '</tr>'
                                );
                            })
                            $.ajax({
                                url: "getCommentPageInfo.do",
                                data: {//请求数据
                                    "tableStatus": ifShowAllComment
                                },
                                type: "post",//请求类型
                                dataType: "json",//响应数据返回格式
                                success: function (data) {//返回成功处理函数
                                    console.log(data);
                                    //alert(data);
                                    var currentPage = data.currentPage + 1;
                                    $("#div_paging").html("");//清空分页按钮
                                    $("#div_paging").append(
                                        '<a>共' + data.recordCount + '条，分' + data.pageCount + '页，当前第' + currentPage + '页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=topPage()>最前页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=prePage()>上一页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=nextPage()>下一页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=endPage()>最尾页</a>'
                                    );
                                },
                                error: function () {//返回失败处理函数
                                    alert("ajax返回失败！");
                                }
                            })
                        }
                    },
                    error:function() {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            }
        });

        //显示待审核评论按钮点击事件
        $("#bt_selectAllBeCheckedComment").click(function () {
            ifShowAllComment = 0;
            $("#bt_selectAllComment").css('display','inline');
            $("#bt_selectAllBeCheckedComment").css('display','none');
            selectAjax("selectAllBeCheckedComment.do");
        });

        //显示所有评论按钮点击事件
        $("#bt_selectAllComment").click(function () {
            ifShowAllComment = 1;
            $("#bt_selectAllBeCheckedComment").css('display','inline');
            $("#bt_selectAllComment").css('display','none');
            selectAjax("selectAllComment.do");
        });

        //上一页点击方法
        function prePage() {
            selectAjax("preCommentPage.do");
        };

        //下一页点击方法
        function nextPage() {
            selectAjax("nextCommentPage.do");
        };

        //最前页点击方法
        function topPage() {
            selectAjax("topCommentPage.do");
        };

        //最尾页点击方法
        function endPage() {
            selectAjax("endCommentPage.do");
        };

        //无参查询请求(用于页面刷新和分页)+内嵌分页信息请求
        function selectAjax(selectURL) {
            $.ajax({
                url:selectURL,
                type:"post",//请求类型
                dataType:"json",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    //console.log(data);
                    $("#tbody_data").html("");//清空数据表
                    if (data.length == 0) alert("没有记录！");
                    else {
                        //alert("查询成功！");
                        var comStatus;
                        $.each(data, function (index, comment) {
                            if (comment.comStatus == 0) comStatus = "待审核";
                            else if (comment.comStatus == -1) comStatus = "不通过";
                            else if (comment.comStatus == 1) comStatus = "通过";
                            $("#tbody_data").append(
                                '<tr>' +
                                '<td>' + comment.comID + '</td>' +
                                '<td>' + comment.comTime + '</td>' +
                                '<td>' + comment.uid + '</td>' +
                                '<td><a href="/manage/goToArticleDetail.do?ArtID=' + comment.artID + '" target="_black">' + comment.artID + '</a></td>' +
                                '<td>' + comment.comContent + '</td>' +
                                '<td>' + comStatus + '</td>' +
                                '<td><a href="javascript:void(0);" onclick=passComment(' + comment.comID + ',' + comment.comStatus + ')>通过</a>&nbsp&nbsp' +
                                '<a href="javascript:void(0);" onclick=notPassComment(' + comment.comID + ',' + comment.comStatus + ')>不通过</a></td>' +
                                '<td><a href="javascript:void(0);" onclick=deleteComment(' + comment.comID + ')>删除</a></td>' +
                                '</tr>'
                            );
                        })
                        $.ajax({
                            url: "getCommentPageInfo.do",
                            data: {//请求数据
                                "tableStatus": ifShowAllComment
                            },
                            type: "post",//请求类型
                            dataType: "json",//响应数据返回格式
                            success: function (data) {//返回成功处理函数
                                console.log(data);
                                //alert(data);
                                var currentPage = data.currentPage + 1;
                                $("#div_paging").html("");//清空分页按钮
                                $("#div_paging").append(
                                    '<a>共' + data.recordCount + '条，分' + data.pageCount + '页，当前第' + currentPage + '页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=topPage()>最前页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=prePage()>上一页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=nextPage()>下一页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=endPage()>最尾页</a>'
                                );
                            },
                            error: function () {//返回失败处理函数
                                alert("ajax返回失败！");
                            }
                        })
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        };

        //审核通过评论方法
        function passComment(ComID, ComStatus) {
            if (ComStatus == '0') {
                $.ajax({
                    url: "passComment.do",//请求路径
                    data: {//请求数据
                        "ComID": ComID
                    },
                    type: "post",//请求类型
                    dataType: "text",//响应数据返回格式
                    success: function (data) {//返回成功处理函数
                        console.log(data);
                        if (data == "databaseFalse") alert("读写数据库失败！");
                        else if (data == "passCommentFalse") alert("审核通过评论失败！");
                        else if (data == "passCommentSuccess") {
                            alert("审核通过评论成功！");
                            selectAjax("refreshComment.do");
                        }
                    },
                    error: function () {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            } else alert("已经审核操作！");
        };

        //审核不通过评论方法
        function notPassComment(ComID, ComStatus) {
            if (ComStatus == '0') {
                $.ajax({
                    url:"notPassComment.do",//请求路径
                    data: {//请求数据
                        "ComID":ComID
                    },
                    type:"post",//请求类型
                    dataType:"text",//响应数据返回格式
                    success:function(data) {//返回成功处理函数
                        console.log(data);
                        if (data == "databaseFalse") alert("读写数据库失败！");
                        else if (data == "notPassCommentFalse") alert("审核不通过评论失败！");
                        else if (data == "notPassCommentSuccess") {
                            alert("审核不通过评论成功！");
                            selectAjax("refreshComment.do");
                        }
                    },
                    error:function() {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            } else alert("已经审核操作！");
        };

        //删除贴文方法
        function deleteComment(ComID) {
            $.ajax({
                url:"deleteComment.do",//请求路径
                data: {//请求数据
                    "ComID":ComID
                },
                type:"post",//请求类型
                dataType:"text",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    console.log(data);
                    if (data == "databaseFalse") alert("读写数据库失败！");
                    else if (data == "deleteCommentFalse") alert("删除评论失败！");
                    else if (data == "deleteCommentSuccess") {
                        alert("删除评论成功！");
                        selectAjax("refreshComment.do");
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        };
    </script>
</html>
