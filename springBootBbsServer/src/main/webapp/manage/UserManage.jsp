<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--http://localhost:8080/manage/UserManage.jsp -->

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>UserManage</title>
        <link rel="stylesheet" href="../css/Manage.css">
        <script src="../js/jquery-3.1.1.min.js"></script>
    </head>

    <body>
        <!--头部-->
        <div id="div_head">
            <!--当前模块名称-->
            <span id="span_modelName" style="display: none">${modelName}</span>

            <!--后台管理系统名称-->
            <span id="span_listIcon"></span>
            <span id="span_bbsSysManage">BBS系统管理后台</span>

            <!--管理员名称-->
            <span id="span_admin">管理员：</span>
            <span id="span_Aname">${Aname}</span>
            <button id="btn_logout" type="button"></button>
        </div>

        <!--导航-->
        <div id="div_navi">
            <!--管理主页页面跳转按钮-->
            <form id="form_goToMainManage" action="goToJSP.do" method="post">
                <span id="span_homeIcon"></span>
                <input type="hidden" name="modelName" value="管理主页"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToMainManage" type="button">管理主页</button>
            </form>

            <!--用户管理页面跳转按钮-->
            <form id="form_goToUserManage" action="goToJSP.do" method="post" style="background: white; opacity: 62%;">
                <span id="span_groupIcon"></span>
                <input type="hidden" name="modelName" value="用户管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToUserManage" type="button" style="color: rgba(32, 160, 255, 1);">用户管理</button>
            </form>

            <!--贴文管理页面跳转按钮-->
            <form id="form_goToArticleManage" action="goToJSP.do" method="post">
                <span id="span_libraryBooksIcon"></span>
                <input type="hidden" name="modelName" value="贴文管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToArticleManage" type="button">贴文管理</button>
            </form>

            <!--评论管理页面跳转按钮-->
            <form id="form_goToCommentManage" action="goToJSP.do" method="post">
                <span id="span_forumIcon"></span>
                <input type="hidden" name="modelName" value="评论管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToCommentManage" type="button">评论管理</button>
            </form>
        </div>

        <!--主体-->
        <div id="div_mainPart">
            <!--查询栏-->
            <div id="div_select">
                <span>选择查询项：</span>
                <!--下拉菜单-->
                <select id="select_selectOption">
                    <option style="display: none" selected>下拉选择</option>
                    <option>用户ID</option>
                    <option>用户名</option>
                    <option>禁言状态</option>
                </select>&nbsp&nbsp&nbsp&nbsp
                <input id="input_selectContent" type="text" placeholder="输入查询内容"/>&nbsp&nbsp&nbsp&nbsp
                <button id="bt_selectUser" type="button">查询</button>
            </div>

            <!--数据栏-->
            <div id="div_data">
                <!--数据以表格形式展示-->
                <table>
                    <thead>
                        <tr>
                            <th>用户ID</th>
                            <th>用户名</th>
                            <th>用户性别</th>
                            <th>用户简介</th>
                            <th>禁言状态</th>
                            <th>禁言到期时间</th>
                            <th>禁言</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_data"></tbody>
                </table>

                <!--分页按钮-->
                </br><div id="div_paging"/>
            </div>
        </div>
    </body>

    <script>
        //ready方法
        $(document).ready(function () {
            selectAjax("selectAllUser.do");
        });

        //后台管理系统图标点击事件
        $("#span_listIcon").click(function () {
            $("#form_goToMainManage").submit();
        });

        //管理主页按钮点击事件
        $("#btn_goToMainManage").click(function() {
            $("#form_goToMainManage").submit();
        });

        //用户管理按钮点击事件
        $("#btn_goToUserManage").click(function() {
            $("#form_goToUserManage").submit();
        });

        //贴文管理按钮点击事件
        $("#btn_goToArticleManage").click(function() {
            $("#form_goToArticleManage").submit();
        });

        //评论管理按钮点击事件
        $("#btn_goToCommentManage").click(function() {
            $("#form_goToCommentManage").submit();
        });

        //注销管理员按钮点击事件
        $("#btn_logout").click(function() {
            alert("注销成功！");
            location.href="AdminLogin.jsp";
        });

        //查询按钮点击事件
        $("#bt_selectUser").click(function () {
            var selectOption = $("#select_selectOption option:selected").val();
            var selectContent = $("#input_selectContent").val();

            if (selectOption == "下拉选择") alert("请选择查询项！");
            else if (selectContent == "") alert("请输入查询内容！");
            else if (selectOption == "用户ID" && isNaN(selectContent)) alert("请输入数字！");
            else if (selectOption == "禁言状态" && (isNaN(selectContent) || parseInt(selectContent) > 3 || parseInt(selectContent) < -1)) alert("请输入-1~3的数字！");
            else {
                if (selectOption == "禁言状态") {
                    selectOption = "禁言等级";
                    switch (selectContent) {
                        case "-1" :
                            selectContent = -1;
                            break;
                        case "0" :
                            selectContent = 3;
                            break;
                        case "1" :
                            selectContent = 2;
                            break;
                        case "2" :
                            selectContent = 1;
                            break;
                        case "3" :
                            selectContent = 0;
                            break;
                    }
                }
                $.ajax({
                    url:"selectUser.do",
                    data: {//请求数据
                        "selectOption":selectOption,
                        "selectContent":selectContent
                    },
                    type:"post",//请求类型
                    dataType:"json",//响应数据返回格式
                    success:function(data) {//返回成功处理函数
                        console.log(data);
                        //alert(selectOption+"  "+selectContent+"  "+data);
                        if (data.length == 0) alert("查询结果为空！");
                        else {
                            alert("查询成功！");
                            $("#tbody_data").html("");//清空数据表
                            var ubanLevel;
                            $.each(data, function (index, user) {
                                switch (user.ubanLevel) {
                                    case -1 :
                                        ubanLevel = "禁言中";
                                        break;
                                    case 0 :
                                        ubanLevel = "剩3次";
                                        break;
                                    case 1 :
                                        ubanLevel = "剩2次";
                                        break;
                                    case 2 :
                                        ubanLevel = "剩1次";
                                        break;
                                    case 3 :
                                        ubanLevel = "剩0次";
                                        break;
                                }
                                if (user.ubanTime == null) user.ubanTime = "未被禁言";
                                $("#tbody_data").append(
                                    '<tr>' +
                                    '<td>' + user.uid + '</td>' +
                                    '<td>' + user.uname + '</td>' +
                                    '<td>' + user.ugender + '</td>' +
                                    '<td>' + user.uint + '</td>' +
                                    '<td>' + ubanLevel + '</td>' +
                                    '<td>' + user.ubanTime + '</td>' +
                                    '<td><a href="javascript:void(0);" onclick=banUserTalk(' + user.uid + ')>禁言</a>&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=allowUserTalk(' + user.uid + ')>解除禁言</a></td>' +
                                    '</tr>'
                                );
                            })
                            $.ajax({
                                url: "getUserPageInfo.do",
                                type: "post",//请求类型
                                dataType: "json",//响应数据返回格式
                                success: function (data) {//返回成功处理函数
                                    console.log(data);
                                    //alert(data);
                                    var currentPage = data.currentPage + 1;
                                    $("#div_paging").html("");//清空分页按钮
                                    $("#div_paging").append(
                                        '<a>共' + data.recordCount + '条，分' + data.pageCount + '页，当前第' + currentPage + '页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=topPage()>最前页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=prePage()>上一页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=nextPage()>下一页</a>&nbsp&nbsp&nbsp' +
                                        '<a href="javascript:void(0);" onclick=endPage()>最尾页</a>'
                                    );
                                },
                                error: function () {//返回失败处理函数
                                    alert("ajax返回失败！");
                                }
                            })
                        }
                    },
                    error:function() {//返回失败处理函数
                        alert("ajax返回失败！");
                    }
                })
            }
        });

        //上一页点击方法
        function prePage() {
            selectAjax("preUserPage.do");
        };

        //下一页点击方法
        function nextPage() {
            selectAjax("nextUserPage.do");
        };

        //最前页点击方法
        function topPage() {
            selectAjax("topUserPage.do");
        };

        //最尾页点击方法
        function endPage() {
            selectAjax("endUserPage.do");
        };

        //无参查询请求(用于页面刷新和分页)+内嵌分页信息请求
        function selectAjax(selectURL) {
            $.ajax({
                url:selectURL,
                type:"post",//请求类型
                dataType:"json",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    //console.log(data);
                    $("#tbody_data").html("");//清空数据表
                    if (data.length == 0) alert("没有记录！");
                    else {
                        //alert("查询成功！");
                        var ubanLevel;
                        $.each(data, function (index, user) {
                            switch (user.ubanLevel) {
                                case -1 :
                                    ubanLevel = "禁言中";
                                    break;
                                case 0 :
                                    ubanLevel = "剩3次";
                                    break;
                                case 1 :
                                    ubanLevel = "剩2次";
                                    break;
                                case 2 :
                                    ubanLevel = "剩1次";
                                    break;
                                case 3 :
                                    ubanLevel = "剩0次";
                                    break;
                            }
                            if (user.ubanTime == null) user.ubanTime = "未被禁言";
                            $("#tbody_data").append(
                                '<tr>' +
                                '<td>' + user.uid + '</td>' +
                                '<td>' + user.uname + '</td>' +
                                '<td>' + user.ugender + '</td>' +
                                '<td>' + user.uint + '</td>' +
                                '<td>' + ubanLevel + '</td>' +
                                '<td>' + user.ubanTime + '</td>' +
                                '<td><a href="javascript:void(0);" onclick=banUserTalk(' + user.uid + ')>禁言</a>&nbsp&nbsp' +
                                '<a href="javascript:void(0);" onclick=allowUserTalk(' + user.uid + ')>解除禁言</a></td>' +
                                '</tr>'
                            );
                        })
                        $.ajax({
                            url: "getUserPageInfo.do",
                            type: "post",//请求类型
                            dataType: "json",//响应数据返回格式
                            success: function (data) {//返回成功处理函数
                                console.log(data);
                                //alert(data);
                                var currentPage = data.currentPage + 1;
                                $("#div_paging").html("");//清空分页按钮
                                $("#div_paging").append(
                                    '<a>共' + data.recordCount + '条，分' + data.pageCount + '页，当前第' + currentPage + '页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=topPage()>最前页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=prePage()>上一页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=nextPage()>下一页</a>&nbsp&nbsp&nbsp' +
                                    '<a href="javascript:void(0);" onclick=endPage()>最尾页</a>'
                                );
                            },
                            error: function () {//返回失败处理函数
                                alert("ajax返回失败！");
                            }
                        })
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        };

        //禁言用户方法
        function banUserTalk(UID) {
            $.ajax({
                url:"banUserTalk.do",//请求路径
                data: {//请求数据
                    "UID":UID
                },
                type:"post",//请求类型
                dataType:"text",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    console.log(data);
                    if (data == "databaseFalse") alert("读写数据库失败！");
                    else if (data == "banUserFalse") alert("禁言失败！");
                    else if (data == "banUserSuccess") {
                        alert("禁言成功！");
                        selectAjax("refreshUser.do");
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        };

        //解除禁言用户方法
        function allowUserTalk(UID) {
            $.ajax({
                url:"allowUserTalk.do",//请求路径
                data: {//请求数据
                    "UID":UID
                },
                type:"post",//请求类型
                dataType:"text",//响应数据返回格式
                success:function(data) {//返回成功处理函数
                    console.log(data);
                    if (data == "databaseFalse") alert("读写数据库失败！");
                    else if (data == "allowUserFalse") alert("解除禁言失败！");
                    else if (data == "allowUserSuccess") {
                        alert("解除禁言成功！");
                        selectAjax("refreshUser.do");
                    }
                },
                error:function() {//返回失败处理函数
                    alert("ajax返回失败！");
                }
            })
        };
    </script>
</html>
