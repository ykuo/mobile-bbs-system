<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--http://localhost:8080/manage/AdminLogin.jsp -->

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>AdminLogin</title>
        <link rel="stylesheet" href="../css/AdminLogin.css">
        <script src="../js/jquery-3.1.1.min.js"></script>
    </head>

    <body>
        <!--登录-->
        <div id="div_Alogin">
            <div id="div_AloginLeft"></div>
            <div id="div_AloginRight">
                <span id="span_actionName">后台管理员登录</span>
                <span id="span_loginTips">如未开通账户请联系项目负责人开通</span>
                <form id="form_Alogin" action="goToJSP.do" method="post">
                    <input type="hidden" name="modelName" value="管理主页"/>
                    <span id="span_personIcon"></span>
                    <input id="input_Aname" name="Aname" type="text" placeholder="输入用户名"/>
                    <span id="span_lockIcon"></span>
                    <input id="input_Apasswd" type="password" placeholder="输入密码"/>
                    <button id="btn_Alogin" type="button">登录</button>
                </form>
            </div>
        </div>
    </body>

    <script>
        //登录按钮点击事件
        $("#btn_Alogin").click(function() {
            var Aname = $("#input_Aname").val();//id选择器
            var Apasswd = $("#input_Apasswd").val();
            if (Aname == "") {
                alert("用户名为空！");
            } else {
                if (Apasswd == "") {
                    alert("密码为空！");
                } else {
                    $.ajax({
                        url:"adminLogin.do",//请求路径
                        data: {//请求数据
                            "Aname":Aname,
                            "Apasswd":Apasswd
                        },
                        type:"post",//请求类型
                        dataType:"text",//响应数据返回格式
                        success:function(data) {//返回成功处理函数
                            console.log(data);
                            //alert("Aname:"+Aname+" "+"Apasswd:"+Apasswd+"\ndata:"+data);
                            if (data == "passwordFalse") alert("密码错误！");
                            else if (data == "adminNotExist") alert("管理员不存在！");
                            else if (data == "loginSuccess") {
                                alert("登录成功！");
                                $("#form_Alogin").submit();
                            }
                        },
                        error:function() {//返回失败处理函数
                            alert("ajax返回失败！");
                        }
                    })
                }
            }
        });
    </script>
</html>
