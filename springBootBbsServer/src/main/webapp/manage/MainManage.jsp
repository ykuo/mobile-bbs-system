<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--http://localhost:8080/manage/MainManage.jsp -->

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>MainManage</title>
        <link rel="stylesheet" href="../css/Manage.css">
        <script src="../js/jquery-3.1.1.min.js"></script>
    </head>

    <body>
        <!--头部-->
        <div id="div_head">
            <!--当前模块名称-->
            <span id="span_modelName" style="display: none">${modelName}</span>

            <!--后台管理系统名称-->
            <span id="span_listIcon"></span>
            <span id="span_bbsSysManage">BBS系统管理后台</span>

            <!--管理员姓名-->
            <span id="span_admin">管理员：</span>
            <span id="span_Aname">${Aname}</span>
            <button id="btn_logout" type="button"></button>
        </div>

        <!--导航-->
        <div id="div_navi">
            <!--管理主页页面跳转按钮-->
            <form id="form_goToMainManage" action="goToJSP.do" method="post" style="background: white; opacity: 62%;">
                <span id="span_homeIcon"></span>
                <input type="hidden" name="modelName" value="管理主页"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToMainManage" type="button" style="color: rgba(32, 160, 255, 1);">管理主页</button>
            </form>

            <!--用户管理页面跳转按钮-->
            <form id="form_goToUserManage" action="goToJSP.do" method="post">
                <span id="span_groupIcon"></span>
                <input type="hidden" name="modelName" value="用户管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToUserManage" type="button">用户管理</button>
            </form>

            <!--贴文管理页面跳转按钮-->
            <form id="form_goToArticleManage" action="goToJSP.do" method="post">
                <span id="span_libraryBooksIcon"></span>
                <input type="hidden" name="modelName" value="贴文管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToArticleManage" type="button">贴文管理</button>
            </form>

            <!--评论管理页面跳转按钮-->
            <form id="form_goToCommentManage" action="goToJSP.do" method="post">
                <span id="span_forumIcon"></span>
                <input type="hidden" name="modelName" value="评论管理"/>
                <input type="hidden" name="Aname" value="${Aname}"/>
                <button id="btn_goToCommentManage" type="button">评论管理</button>
            </form>
        </div>

        <!--主体-->
        <div id="div_mainPart"></div>
    </body>

    <script>
        //后台管理系统图标点击事件
        $("#span_listIcon").click(function () {
            $("#form_goToMainManage").submit();
        });

        //管理主页按钮点击事件
        $("#btn_goToMainManage").click(function() {
            $("#form_goToMainManage").submit();
        });

        //用户管理按钮点击事件
        $("#btn_goToUserManage").click(function() {
            $("#form_goToUserManage").submit();
        });

        //贴文管理按钮点击事件
        $("#btn_goToArticleManage").click(function() {
            $("#form_goToArticleManage").submit();
        });

        //评论管理按钮点击事件
        $("#btn_goToCommentManage").click(function() {
            $("#form_goToCommentManage").submit();
        });

        //注销管理员按钮点击事件
        $("#btn_logout").click(function() {
            alert("注销成功！");
            location.href="AdminLogin.jsp";
        });
    </script>
</html>
