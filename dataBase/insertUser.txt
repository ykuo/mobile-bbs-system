insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('李二', 'def456', '女', '我是李二，我是一名医生。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('张三', '123456', '男', '我是张三，欢迎关注我的个人主页。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('李四', '654321', '女', '我是李四，我喜欢旅行和摄影。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('王五', '111222', '男', '我是王五，我喜欢读书和运动。', 1, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('赵六', '333444', '女', '我是赵六，我是一名设计师。', 0, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('陈七', '555666', '男', '我是陈七，我喜欢音乐和瑜伽。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('刘八', '777888', '女', '我是刘八，我喜欢美食和旅行。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('周九', '999000', '男', '我是周九，我是一名软件工程师。', 1, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('吴十', 'abc123', '女', '我是吴十，我喜欢摄影和旅行。', 0, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('郑十一', 'def456', '男', '我是郑十一，我喜欢健身和读书。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('孙十二', 'ghi789', '女', '我是孙十二，我是一名教师。', 1, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('曹十三', 'jkl123', '男', '我是曹十三，我喜欢画画和旅行。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('黄十四', 'mno456', '女', '我是黄十四，我是一名医生。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('杨十五', 'pqr789', '男', '我是杨十五，我喜欢运动和音乐。', 0, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('徐十六', 'stu012', '女', '我是徐十六，我喜欢读书和旅行。', 1, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('朱十七', 'vwx345', '男', '我是朱十七，我是一名工程师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('胡十八', 'yz123', '女', '我是胡十八，我喜欢摄影和瑜伽。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('林十九', '456789', '男', '我是林十九，我喜欢旅行和音乐。', 0, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('郭二十', '987654', '女', '我是郭二十，我是一名设计师。', 1, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('蔡二十一', '321654', '男', '我是蔡二十一，我喜欢读书和旅行。', 0, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('朱二十二', '321654', '男', '我是朱二十二，我喜欢画画和音乐。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('赵二十三', 'jkl012', '女', '我是赵二十三，我是一名工程师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('刘二十四', 'mno345', '男', '我是刘二十四，我喜欢读书和旅行。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('陈二十五', 'pqr678', '女', '我是陈二十五，我是一名设计师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('杨二十六', 'stu901', '男', '我是杨二十六，我喜欢摄影和音乐。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('吴二十七', 'vwx234', '女', '我是吴二十七，我是一名教师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('许二十八', 'yz567', '男', '我是许二十八，我喜欢画画和电影。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('何二十九', '321098', '女', '我是何二十九，我喜读书和瑜伽。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('刘三十', 'xyz456', '男', '我是刘三十，我是一名律师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('韩三十一', 'lmn789', '女', '我是韩三十一，我喜欢写作和旅行。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('冯三十二', 'uvw012', '男', '我是冯三十二，我是一名医生。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('曹三十三', 'opq345', '女', '我是曹三十三，我喜欢篮球和音乐。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('魏三十四', 'rst678', '男', '我是魏三十四，我是一名工程师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('朱三十五', 'klm901', '女', '我是朱三十五，我喜欢读书和旅行。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('石三十六', 'pqr234', '男', '我是石三十六，我是一名设计师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('陈三十七', 'stu567', '女', '我是陈三十七，我喜欢摄影和音乐。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('赵三十八', 'vwx890', '男', '我是赵三十八，我是一名教师。', 1, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('曹三十九', 'o1qgp5', '男', '我是曹三十九，我喜欢篮球和音乐。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('钱四十', 'yz012', '女', '我是钱四十，我喜欢唱跳和篮球。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('曹四十一', 'opq3m5', '女', '我是曹四十一，我喜欢篮球和音乐。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('杨四十二', 'swu9w1', '男', '我是杨四十二，我喜欢摄影和音乐。', 1, 0, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('吴四十三', 'vwx2e5', '女', '我是吴四十三，我是一名教师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('陈四十四', 'pq5278', '女', '我是陈四十四，我是一名设计师。', 0, 1, 0);

insert into User (Uname, Upasswd, Ugender, Uint, UfavorPrivacy, UcollPrivacy, UbanLevel) 
values ('赵四十五', 'jk2712', '女', '我是赵四十五，我是一名工程师。', 0, 1, 0);
