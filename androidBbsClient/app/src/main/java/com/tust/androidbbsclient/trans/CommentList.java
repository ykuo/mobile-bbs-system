package com.tust.androidbbsclient.trans;

/**
 * 通信类：评论列表
 * 属性：贴文ID 实型 非空 外键
 * 属性：贴文类别 变长字符20 非空
 * 属性：贴文标题 变长字符200 非空
 * 属性：评论内容 变长字符600 非空
 * 属性：评论时间 日期时间型 非空
 * 属性：评论审核状态 小实型 非空 自定义约束（0or1or-1） 规定默认0待审核，1通过，-1不通过
 */
public class CommentList {
    private int ArtID;
    private String ArtCategory;
    private String ArtTitle;
    private String ComContent;
    private String ComTime;
    private int ComStatus;

    public CommentList(int artID, String artCategory, String artTitle, String comContent, String comTime, int comStatus) {
        ArtID = artID;
        ArtCategory = artCategory;
        ArtTitle = artTitle;
        ComContent = comContent;
        ComTime = comTime;
        ComStatus = comStatus;
    }

    @Override
    public String toString() {
        return "CommentList{" +
                "ArtID=" + ArtID +
                ", ArtCategory='" + ArtCategory + '\'' +
                ", ArtTitle='" + ArtTitle + '\'' +
                ", ComContent='" + ComContent + '\'' +
                ", ComTime='" + ComTime + '\'' +
                ", ComStatus=" + ComStatus +
                '}';
    }

    public int getArtID() {
        return ArtID;
    }

    public void setArtID(int artID) {
        ArtID = artID;
    }

    public String getArtCategory() {
        return ArtCategory;
    }

    public void setArtCategory(String artCategory) {
        ArtCategory = artCategory;
    }

    public String getArtTitle() {
        return ArtTitle;
    }

    public void setArtTitle(String artTitle) {
        ArtTitle = artTitle;
    }

    public String getComContent() {
        return ComContent;
    }

    public void setComContent(String comContent) {
        ComContent = comContent;
    }

    public String getComTime() {
        return ComTime;
    }

    public void setComTime(String comTime) {
        ComTime = comTime;
    }

    public int getComStatus() {
        return ComStatus;
    }

    public void setComStatus(int comStatus) {
        ComStatus = comStatus;
    }
}
