package com.tust.androidbbsclient.tool;

import android.content.Context;

import com.tust.androidbbsclient.domain.User;

public class Application extends android.app.Application {
    public User user;
    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public void addUser(User user) {
        this.user = new User(user.getUID(), user.getUname(), user.getUpasswd(), user.getUgender(), user.getUint(), user.getUfavorPrivacy(), user.getUcollPrivacy(), user.getUbanLevel(), user.getUbanTime());
    }

    public Context getContext() {
        return context;
    }
}
