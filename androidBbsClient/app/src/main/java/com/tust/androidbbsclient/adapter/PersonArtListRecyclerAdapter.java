package com.tust.androidbbsclient.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tust.androidbbsclient.ArticleDetailActivity;
import com.tust.androidbbsclient.R;
import com.tust.androidbbsclient.trans.ArticleList;
import com.tust.androidbbsclient.tool.Tool;

import java.util.List;

public class PersonArtListRecyclerAdapter extends RecyclerView.Adapter<PersonArtListViewHolder> {
    private Toast toast;
    private Context context;
    private Activity activity;
    private List<ArticleList> articleLists;//贴文列表不包括贴文内容和图片

    //有参构造函数
    public PersonArtListRecyclerAdapter(Context context, Activity activity, List<ArticleList> articleLists) {
        this.context = context;
        this.activity = activity;
        this.articleLists = articleLists;
    }

    @NonNull
    @Override
    public PersonArtListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycleitem_person_artlist, parent, false);
        PersonArtListViewHolder personArtListViewHolder = new PersonArtListViewHolder(view);

        return personArtListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonArtListViewHolder holder, int position) {
        ArticleList itemdata = articleLists.get(position);

        //填充数据源
        holder.textViewArtListArtID.setText(String.valueOf(itemdata.getArtID()));
        holder.textViewArtListArtAuthor.setText(itemdata.getUname());
        if (itemdata.getArtStatus() == 0) {
            holder.textViewArtListArtStatus.setText("待审核");
            holder.textViewArtListArtStatus.setTextColor(ContextCompat.getColor(context, R.color.powderblue));
        }
        else if (itemdata.getArtStatus() == 1) holder.textViewArtListArtStatus.setText("审核通过");
        else if (itemdata.getArtStatus() == -1) {
            holder.textViewArtListArtStatus.setText("审核不通过");
            holder.textViewArtListArtStatus.setTextColor(ContextCompat.getColor(context, R.color.sandybrown));
        } else if (itemdata.getArtStatus() == -2) holder.textViewArtListArtStatus.setText("");
        holder.textViewArtListArtSubTime.setText(itemdata.getArtSubTime());
        holder.textViewArtListArtCategory.setText(itemdata.getArtCategory());
        holder.textViewArtListArtTitle.setText(itemdata.getArtTitle());
        holder.textViewArtListArtFavorNum.setText(String.valueOf(itemdata.getArtFavorNum()));
        holder.textViewArtListArtCollNum.setText(String.valueOf(itemdata.getArtCollNum()));
        holder.textViewArtListArtComNum.setText(String.valueOf(itemdata.getArtComNum()));

        //RecyclerView子项监听事件
        holder.linearLayoutArtListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToast(Tool.addEmoji() + "正在打开贴文！");

                //跳转活动，带有ArtID参数
                Intent intent = new Intent(context, ArticleDetailActivity.class);
                intent.putExtra("ArtID", Integer.valueOf(holder.textViewArtListArtID.getText().toString()));
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_out,R.anim.fade_in);//改变跳转活动动画，淡出淡入
            }
        });
    }

    @Override
    public int getItemCount() {
        return articleLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    //Toast消息提示框显示方法
    private void showToast(String s) {
        if (toast == null){
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }else {
            toast.cancel();
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }
        toast.show();
    }
}

//ViewHolder类
class PersonArtListViewHolder extends RecyclerView.ViewHolder {
    //数据视图组件
    LinearLayout linearLayoutArtListItem;
    TextView textViewArtListArtID;
    TextView textViewArtListArtAuthor;
    TextView textViewArtListArtStatus;
    TextView textViewArtListArtSubTime;
    TextView textViewArtListArtCategory;
    TextView textViewArtListArtTitle;
    TextView textViewArtListArtFavorNum;
    TextView textViewArtListArtComNum;
    TextView textViewArtListArtCollNum;

    public PersonArtListViewHolder(@NonNull View itemView) {
        super(itemView);

        //绑定RecyclerView子项的控件
        linearLayoutArtListItem = itemView.findViewById(R.id.ll_artListItem);
        textViewArtListArtID = itemView.findViewById(R.id.tv_artListArtID);
        textViewArtListArtAuthor = itemView.findViewById(R.id.tv_artListArtAuthor);
        textViewArtListArtStatus = itemView.findViewById(R.id.tv_artListArtStatus);
        textViewArtListArtSubTime = itemView.findViewById(R.id.tv_artListArtSubTime);
        textViewArtListArtCategory = itemView.findViewById(R.id.tv_artListArtCategory);
        textViewArtListArtTitle = itemView.findViewById(R.id.tv_artListArtTitle);
        textViewArtListArtFavorNum = itemView.findViewById(R.id.tv_artListArtFavorNum);
        textViewArtListArtComNum = itemView.findViewById(R.id.tv_artListArtComNum);
        textViewArtListArtCollNum = itemView.findViewById(R.id.tv_artListArtCollNum);
    }
}
