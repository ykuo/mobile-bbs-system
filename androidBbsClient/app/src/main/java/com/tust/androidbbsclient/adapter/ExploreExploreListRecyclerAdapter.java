package com.tust.androidbbsclient.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.tust.androidbbsclient.ArticleDetailActivity;
import com.tust.androidbbsclient.R;
import com.tust.androidbbsclient.tool.Tool;
import com.tust.androidbbsclient.trans.ExploreList;

import java.util.List;

public class ExploreExploreListRecyclerAdapter extends RecyclerView.Adapter<ExploreExploreListViewHolder> {
    private Toast toast;
    private Context context;
    private Fragment fragment;
    private List<ExploreList> exploreLists;

    //有参构造函数
    public ExploreExploreListRecyclerAdapter(Context context, Fragment fragment, List<ExploreList> exploreLists) {
        this.context = context;
        this.fragment = fragment;
        this.exploreLists = exploreLists;
    }

    @NonNull
    @Override
    public ExploreExploreListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycleitem_explore_explorelist, parent, false);
        ExploreExploreListViewHolder exploreExploreListViewHolder = new ExploreExploreListViewHolder(view);

        return exploreExploreListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExploreExploreListViewHolder holder, int position) {
        ExploreList itemdata = exploreLists.get(position);

        //填充数据源
        holder.textViewExploreListArtID.setText(String.valueOf(itemdata.getArtID()));
        holder.textViewExploreListUname.setText(itemdata.getUname());
        holder.textViewExploreListArtSubTime.setText(itemdata.getArtSubTime());
        holder.textViewExploreListArtTitle.setText(itemdata.getArtTitle());
        holder.textViewExploreListArtCategory.setText(itemdata.getArtCategory());
        holder.textViewExploreListArtContent.setText(itemdata.getArtContent());
        holder.textViewExploreListArtFavorNum.setText(String.valueOf(itemdata.getArtFavorNum()));
        holder.textViewExploreListArtComNum.setText(String.valueOf(itemdata.getArtComNum()));
        holder.textViewExploreListArtCollNum.setText(String.valueOf(itemdata.getArtCollNum()));
        if (itemdata.getArtPic1() != null) holder.imageViewExploreListArtPic1.setImageBitmap(BitmapFactory.decodeByteArray(itemdata.getArtPic1(), 0, itemdata.getArtPic1().length, getBitmapOptions(itemdata.getArtPic1().length)));

        //RecyclerView子项监听事件
        holder.linearLayoutExploreListArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToast(Tool.addEmoji() + "正在打开贴文！");

                //跳转活动，带有ArtID参数
                Intent intent = new Intent(context, ArticleDetailActivity.class);
                intent.putExtra("ArtID", Integer.valueOf(holder.textViewExploreListArtID.getText().toString()));
                context.startActivity(intent);
                fragment.getActivity().overridePendingTransition(R.anim.fade_out,R.anim.fade_in);//改变跳转活动动画，淡出淡入
            }
        });
    }

    //按照图像数据大小给出压缩倍率
    public BitmapFactory.Options getBitmapOptions(int length) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        int targetLength = 600 * 1024;//单位字节（B）

        if (length < targetLength) options.inSampleSize = 1;
        else if (length < targetLength * 2) options.inSampleSize = 1;
        else if (length < targetLength * 4) options.inSampleSize = 4;
        else if (length < targetLength * 8) options.inSampleSize = 8;
        else if (length < targetLength * 16) options.inSampleSize = 16;
        else if (length < targetLength * 32) options.inSampleSize = 32;
        else if (length < targetLength * 64) options.inSampleSize = 64;
        else options.inSampleSize = 128;

        return options;
    }

    @Override
    public int getItemCount() {
        return exploreLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    //Toast消息提示框显示方法
    private void showToast(String s) {
        if (toast == null){
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }else {
            toast.cancel();
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }
        toast.show();
    }
}

//ViewHolder类
class ExploreExploreListViewHolder extends RecyclerView.ViewHolder {
    //数据视图组件
    LinearLayout linearLayoutExploreListArticle;
    TextView textViewExploreListArtID;
    TextView textViewExploreListUname;
    TextView textViewExploreListArtSubTime;
    TextView textViewExploreListArtTitle;
    TextView textViewExploreListArtCategory;
    TextView textViewExploreListArtContent;
    TextView textViewExploreListArtFavorNum;
    TextView textViewExploreListArtComNum;
    TextView textViewExploreListArtCollNum;
    ImageView imageViewExploreListArtPic1;

    public ExploreExploreListViewHolder(@NonNull View itemView) {
        super(itemView);

        //绑定RecyclerView子项的控件
        linearLayoutExploreListArticle = itemView.findViewById(R.id.ll_exploreListArticle);
        textViewExploreListArtID = itemView.findViewById(R.id.tv_exploreListArtID);
        textViewExploreListUname = itemView.findViewById(R.id.tv_exploreListUname);
        textViewExploreListArtSubTime = itemView.findViewById(R.id.tv_exploreListArtSubTime);
        textViewExploreListArtTitle = itemView.findViewById(R.id.tv_exploreListArtTitle);
        textViewExploreListArtCategory = itemView.findViewById(R.id.tv_exploreListArtCategory);
        textViewExploreListArtContent = itemView.findViewById(R.id.tv_exploreListArtContent);
        textViewExploreListArtFavorNum = itemView.findViewById(R.id.tv_exploreListArtFavorNum);
        textViewExploreListArtComNum = itemView.findViewById(R.id.tv_exploreListArtComNum);
        textViewExploreListArtCollNum = itemView.findViewById(R.id.tv_exploreListArtCollNum);
        imageViewExploreListArtPic1 = itemView.findViewById(R.id.iv_exploreListArtPic1);
    }
}
