package com.tust.androidbbsclient.trans;

import java.util.Arrays;

/**
 * 通信类：浏览列表
 * 属性：贴文ID 实型 自动增长 主键
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：贴文发布时间 日期时间型 非空
 * 属性：贴文类别 变长字符20 非空
 * 属性：贴文标题 变长字符200 非空
 * 属性：贴文文字 变长字符2000 非空
 * 属性：贴文图片1 长二进制
 * 属性：贴文点赞量 实型 非空
 * 属性：贴文收藏量 实型 非空
 * 属性：贴文评论量 实型 非数据库
 */
public class ExploreList {
    private int ArtID;
    private String Uname;
    private String ArtSubTime;
    private String ArtCategory;
    private String ArtTitle;
    private String ArtContent;
    private byte[] ArtPic1;
    private int ArtFavorNum;
    private int ArtCollNum;
    private int ArtComNum;

    public ExploreList(int artID, String uname, String artSubTime, String artCategory, String artTitle, String artContent, byte[] artPic1, int artFavorNum, int artCollNum, int artComNum) {
        ArtID = artID;
        Uname = uname;
        ArtSubTime = artSubTime;
        ArtCategory = artCategory;
        ArtTitle = artTitle;
        ArtContent = artContent;
        ArtPic1 = artPic1;
        ArtFavorNum = artFavorNum;
        ArtCollNum = artCollNum;
        ArtComNum = artComNum;
    }

    @Override
    public String toString() {
        return "ExploreList{" +
                "ArtID=" + ArtID +
                ", Uname='" + Uname + '\'' +
                ", ArtSubTime='" + ArtSubTime + '\'' +
                ", ArtCategory='" + ArtCategory + '\'' +
                ", ArtTitle='" + ArtTitle + '\'' +
                ", ArtContent='" + ArtContent + '\'' +
                ", ArtPic1=" + Arrays.toString(ArtPic1) +
                ", ArtFavorNum=" + ArtFavorNum +
                ", ArtCollNum=" + ArtCollNum +
                ", ArtComNum=" + ArtComNum +
                '}';
    }

    public int getArtID() {
        return ArtID;
    }

    public void setArtID(int artID) {
        ArtID = artID;
    }

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }

    public String getArtSubTime() {
        return ArtSubTime;
    }

    public void setArtSubTime(String artSubTime) {
        ArtSubTime = artSubTime;
    }

    public String getArtCategory() {
        return ArtCategory;
    }

    public void setArtCategory(String artCategory) {
        ArtCategory = artCategory;
    }

    public String getArtTitle() {
        return ArtTitle;
    }

    public void setArtTitle(String artTitle) {
        ArtTitle = artTitle;
    }

    public String getArtContent() {
        return ArtContent;
    }

    public void setArtContent(String artContent) {
        ArtContent = artContent;
    }

    public byte[] getArtPic1() {
        return ArtPic1;
    }

    public void setArtPic1(byte[] artPic1) {
        ArtPic1 = artPic1;
    }

    public int getArtFavorNum() {
        return ArtFavorNum;
    }

    public void setArtFavorNum(int artFavorNum) {
        ArtFavorNum = artFavorNum;
    }

    public int getArtCollNum() {
        return ArtCollNum;
    }

    public void setArtCollNum(int artCollNum) {
        ArtCollNum = artCollNum;
    }

    public int getArtComNum() {
        return ArtComNum;
    }

    public void setArtComNum(int artComNum) {
        ArtComNum = artComNum;
    }
}
