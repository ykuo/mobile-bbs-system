package com.tust.androidbbsclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.WindowManager;

//使用自定义theme去除冷启动状态栏，需要继承系统级Activity
public class StartActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //if (getSupportActionBar() != null) getSupportActionBar().hide();//隐藏标题导航栏(已在theme中将DarkActionBar换为NoActionBar实现相同功能)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//去除时间和电量等状态栏

        //全面屏适配
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
            getWindow().setAttributes(lp);
        }

        int second = 2;//倒计时2s,跳转到LoginActivity
        new CountDownTimer(second * 1000, 1000) {

            @Override
            public void onTick(long l) {
                Log.e("Timer", "剩余时间:" + l);
            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_out, R.anim.fade_in);//改变跳转活动动画，淡出淡入
            }
        }.start();
    }
}
