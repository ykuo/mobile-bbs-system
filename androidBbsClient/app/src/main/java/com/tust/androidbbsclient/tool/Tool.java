package com.tust.androidbbsclient.tool;

import android.annotation.SuppressLint;
import android.widget.TextView;

import java.util.Random;

public class Tool {
    public static final String WiFiSSID = "Redmi_E786";//使用内网地址时连接的WiFi名称
    public static String InetAddress;//统一保存服务器的External访问地址
    public static final String InetAddressLocal = "http://192.168.31.10:8080";//宿舍服务器内网地址
    public static final String InetAddressPenetration = "http://192.168.31.10:8080";//服务器外网地址

    public static String selectedCategory;//保存浏览列表的当前选中类别
    @SuppressLint("StaticFieldLeak")
    public static TextView oldTextViewCategoryListArtCategory;//保存类别列表上一次点击的TextView

    //解决Java中String.length()方法视1个中文为1个字符的问题
    public static int StringLength(String value) {
        int valueLength = 0;
        String chinese = "[\u4e00-\u9fa5]";
        for (int i = 0; i < value.length(); i++) {
            String temp = value.substring(i, i + 1);
            if (temp.matches(chinese)) valueLength += 2;
            else valueLength += 1;
        }
        return valueLength;
    }

    //随机增加emoji图标，增加用户交互趣味性
    public static String addEmoji() {
        String emoji = null;
        Random random = new Random();//新建无参随机数种子
        int sign = random.nextInt(50);//随机生成0~49数字

        switch (sign) {
            case 0:
                emoji = "\uD83D\uDE48";
                break;
            case 1:
                emoji = "\uD83D\uDCA5";
                break;
            case 2:
                emoji = "\uD83D\uDCA6";
                break;
            case 3:
                emoji = "\uD83D\uDC36";
                break;
            case 4:
                emoji = "\uD83D\uDC3A";
                break;
            case 5:
                emoji = "\uD83E\uDD8A";
                break;
            case 6:
                emoji = "\uD83E\uDD5A";
                break;
            case 7:
                emoji = "\uD83D\uDC31";
                break;
            case 8:
                emoji = "\uD83E\uDD81";
                break;
            case 9:
                emoji = "\uD83D\uDC2F";
                break;
            case 10:
                emoji = "\uD83E\uDD84";
                break;
            case 11:
                emoji = "\uD83D\uDC2E";
                break;
            case 12:
                emoji = "\uD83D\uDC39";
                break;
            case 13:
                emoji = "\uD83D\uDC30";
                break;
            case 14:
                emoji = "\uD83D\uDC3B";
                break;
            case 15:
                emoji = "\uD83D\uDC28";
                break;
            case 16:
                emoji = "\uD83D\uDC3C";
                break;
            case 17:
                emoji = "\uD83C\uDF49";
                break;
            case 18:
                emoji = "\uD83D\uDC3E";
                break;
            case 19:
                emoji = "\uD83D\uDC14";
                break;
            case 20:
                emoji = "\uD83D\uDC25";
                break;
            case 21:
                emoji = "\uD83D\uDC27";
                break;
            case 22:
                emoji = "\uD83C\uDF84";
                break;
            case 23:
                emoji = "\uD83E\uDD89";
                break;
            case 24:
                emoji = "\uD83C\uDF45";
                break;
            case 25:
                emoji = "\uD83D\uDC38";
                break;
            case 26:
                emoji = "\uD83D\uDC22";
                break;
            case 27:
                emoji = "\uD83D\uDC0D";
                break;
            case 28:
                emoji = "\uD83D\uDC09";
                break;
            case 29:
                emoji = "\uD83E\uDD96";
                break;
            case 30:
                emoji = "\uD83D\uDC33";
                break;
            case 31:
                emoji = "\uD83D\uDC2C";
                break;
            case 32:
                emoji = "\uD83D\uDC1F";
                break;
            case 33:
                emoji = "\uD83E\uDD88";
                break;
            case 34:
                emoji = "\uD83D\uDC19";
                break;
            case 35:
                emoji = "\uD83C\uDF4E";
                break;
            case 36:
                emoji = "\uD83C\uDF4A";
                break;
            case 37:
                emoji = "\uD83D\uDC90";
                break;
            case 38:
                emoji = "\uD83C\uDF38";
                break;
            case 39:
                emoji = "\uD83C\uDF3B";
                break;
            case 40:
                emoji = "\uD83C\uDF37";
                break;
            case 41:
                emoji = "\uD83C\uDF32";
                break;
            case 42:
                emoji = "\uD83C\uDF35";
                break;
            case 43:
                emoji = "\uD83C\uDF40";
                break;
            case 44:
                emoji = "\uD83C\uDF41";
                break;
            case 45:
                emoji = "\uD83C\uDF44";
                break;
            case 46:
                emoji = "\uD83C\uDF30";
                break;
            case 47:
                emoji = "\uD83E\uDD80";
                break;
            case 48:
                emoji = "\uD83E\uDD90";
                break;
            case 49:
                emoji = "\uD83C\uDF08";
                break;
        }

        return emoji;
    }
}
