package com.tust.androidbbsclient;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tust.androidbbsclient.tool.Tool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {
    private Toast toast;
    private long exitTime;
    private String Uname;

    //权限获取启动器
    ActivityResultLauncher<String[]> activityResultLauncher = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), result -> {//注册权限获取启动器
        if (result.get(Manifest.permission.INTERNET) != null) {//检查是否已动态获取⽹络权限
            if (Objects.requireNonNull(result.get(Manifest.permission.INTERNET)).equals(true)) ;//showToast( Tool.addEmoji() + "⽹络权限获取成功！");
            else showToast(Tool.addEmoji() + "⽹络权限获取失败！");
            System.out.println("权限：⽹络连接权限" + Objects.requireNonNull(result.get(Manifest.permission.INTERNET)));
        }

        if (result.get(Manifest.permission.ACCESS_FINE_LOCATION) != null) {//检查是否已动态获取定位权限
            if (Objects.requireNonNull(result.get(Manifest.permission.ACCESS_FINE_LOCATION)).equals(true)) ;//showToast( Tool.addEmoji() + "精确定位权限获取成功！");
            else showToast(Tool.addEmoji() + "精确定位权限获取失败！");
            System.out.println("权限：精确定位权限" + Objects.requireNonNull(result.get(Manifest.permission.ACCESS_FINE_LOCATION)));
        }

        if (result.get(Manifest.permission.ACCESS_WIFI_STATE) != null) {//检查是否已动态获取WiFi权限
            if (Objects.requireNonNull(result.get(Manifest.permission.ACCESS_WIFI_STATE)).equals(true)) ;//showToast( Tool.addEmoji() + "WiFi权限获取成功！");
            else showToast(Tool.addEmoji() + "WiFi权限获取失败！");
            System.out.println("权限：WiFi信息权限" + Objects.requireNonNull(result.get(Manifest.permission.ACCESS_WIFI_STATE)));

            //获取当前Wifi名称，选择服务器地址
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo.getSSID().equals("\"" + Tool.WiFiSSID + "\"")) Tool.InetAddress = Tool.InetAddressLocal;
            else Tool.InetAddress = Tool.InetAddressPenetration;
            //showToast( Tool.addEmoji() + "服务器地址:" + Tool.InetAddress);
            System.out.println("WiFi:" + wifiInfo.getSSID());
            System.out.println("InetAddress" + Tool.InetAddress);
        }
    });

    //用于在线程间传递消息
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);//获取子线程传递的消息
            if (msg.what == 1) {//登录请求返回值
                String strMsg = msg.obj.toString();
                System.out.println("用户登录：" + strMsg);

                switch (strMsg) {
                    case "serverFalse":
                        showToast(Tool.addEmoji() + "服务器通信失败！");
                        break;
                    case "userNotExist":
                        showToast(Tool.addEmoji() + "用户不存在！");
                        break;
                    case "passwordFalse":
                        showToast(Tool.addEmoji() + "密码错误！");
                        break;
                    case "loginSuccess":
                        showToast(Tool.addEmoji() + "登陆成功！");

                        //跳转活动
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("Uname", Uname);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_out, R.anim.fade_in);//改变跳转活动动画，淡出淡入
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //调用权限获取启动器动态申请访问⽹络连接权限
        activityResultLauncher.launch(new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE});

        EditText editTextUname = this.findViewById(R.id.et_uname);
        EditText editTextUpasswd = this.findViewById(R.id.et_upasswd);
        TextView editTextGoToRegister = this.findViewById(R.id.tv_goToRegister);
        Button buttonLogin = this.findViewById(R.id.btn_login);

        //登录按钮监听事件
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uname = editTextUname.getText().toString();
                String Upasswd = editTextUpasswd.getText().toString();

                //输入值的前端判断
                if (Uname.equals("")) showToast(Tool.addEmoji() + "请输入用户名！");
                else if (Tool.StringLength(Uname) > 20) showToast(Tool.addEmoji() + "用户名小于20字符！");
                else if (Upasswd.equals("")) showToast(Tool.addEmoji() + "请输入密码！");
                else if (Tool.StringLength(Upasswd) > 10) showToast(Tool.addEmoji() + "密码小于10字符！");
                else {
                    //发送登录请求子线程
                    new Thread() {
                        @Override
                        public void run() {
                            OutputStream outputStream = null;
                            BufferedReader bufferedReader = null;
                            String data = "";
                            try {
                                Message handlerMsg = Message.obtain();//构造Message对象，用于向主线程传递消息
                                handlerMsg.what = 1;//增加handlerMsg标识符

                                URL url = new URL(Tool.InetAddress + "/userLogin.do");//请求地址
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                                //连接类相关设置
                                connection.setRequestMethod("POST");
                                connection.setConnectTimeout(3000);//连接服务器超时
                                connection.setReadTimeout(3000);//从服务器读取数据超时
                                connection.setDoInput(true);//允许写入，从服务端读取结果流
                                connection.setDoOutput(true);//允许写出，开启Post请求体
                                connection.setUseCaches(false);//不使用Cache
                                connection.setInstanceFollowRedirects(true);//自动处理重定向，成功获得200状态码
                                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");//设置http请求头格式

                                connection.connect();//连接网络
                                String params = "Uname=" + Uname + "&Upasswd=" + Upasswd;//请求体内容
                                outputStream = connection.getOutputStream();//请求输出流
                                outputStream.write(params.getBytes());

                                int code = connection.getResponseCode();//获得响应状态码
                                if (code == 200) {//返回状态码200，连接成功
                                    bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                    data = bufferedReader.readLine();

                                    handlerMsg.obj = data;//写入线程传递消息
                                } else handlerMsg.obj = "serverFalse";//服务器通信失败
                                connection.disconnect();//关闭连接

                                handler.sendMessage(handlerMsg);//发送线程消息
                            } catch (IOException e) {
                                Message handlerMsg = Message.obtain();//构造Message对象，用于向主线程传递消息
                                handlerMsg.what = 1;//增加handlerMsg标识符
                                handlerMsg.obj = "serverFalse";
                                handler.sendMessage(handlerMsg);

                                //throw new RuntimeException(e);
                            } finally {
                                try {
                                    if (bufferedReader != null) bufferedReader.close();
                                    if (outputStream != null) {
                                        outputStream.flush();
                                        outputStream.close();
                                    }
                                } catch (IOException e) {
                                    System.out.println("子线程：finally失败");
                                    //throw new RuntimeException(e);
                                }
                            }
                        }
                    }.start();
                }
            }
        });

        //注册文本监听事件
        editTextGoToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_out,R.anim.fade_in);//改变跳转活动动画，淡出淡入
            }
        });
    }

    //返回监听事件
    @Override
    public void onBackPressed() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            showToast(Tool.addEmoji() + "再按一次退出程序");
            exitTime = System.currentTimeMillis();
        } else {
            this.finishAffinity();
        }
    }

    //Toast消息提示框显示方法
    private void showToast(String s) {
        if (toast == null){
            toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
        }else {
            toast.cancel();
            toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
        }
        toast.show();
    }
}
