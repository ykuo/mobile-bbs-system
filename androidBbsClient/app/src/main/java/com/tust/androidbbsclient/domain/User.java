package com.tust.androidbbsclient.domain;

/**
 * 实体类：用户
 * 属性：用户ID 实型 自动增长 主键
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：用户密码 变长字符10 非空
 * 属性：用户性别 字符2 自定义约束（男or女）
 * 属性：用户简介 变长字符600
 * 属性：用户点赞隐私 小实型 非空 自定义约束（0or1） 规定默认0显示，1不显示
 * 属性：用户收藏隐私 小实型 非空 自定义约束（0or1） 规定默认0显示，1不显示
 * 属性：用户禁言等级 小实型 非空 规定默认0不禁言，审核不通过+1，若>=3禁言1天，并置-1禁言
 * 属性：用户禁言到期时间 日期时间型 默认null
 */
public class User {
    private int UID;
    private String Uname;
    private String Upasswd;
    private String Ugender;
    private String Uint;
    private int UfavorPrivacy;
    private int UcollPrivacy;
    private int UbanLevel;
    private String UbanTime;

    public User(int UID, String uname, String upasswd, String ugender, String uint, int ufavorPrivacy, int ucollPrivacy, int ubanLevel, String ubanTime) {
        this.UID = UID;
        Uname = uname;
        Upasswd = upasswd;
        Ugender = ugender;
        Uint = uint;
        UfavorPrivacy = ufavorPrivacy;
        UcollPrivacy = ucollPrivacy;
        UbanLevel = ubanLevel;
        UbanTime = ubanTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "UID=" + UID +
                ", Uname='" + Uname + '\'' +
                ", Upasswd='" + Upasswd + '\'' +
                ", Ugender='" + Ugender + '\'' +
                ", Uint='" + Uint + '\'' +
                ", UfavorPrivacy=" + UfavorPrivacy +
                ", UcollPrivacy=" + UcollPrivacy +
                ", UbanLevel=" + UbanLevel +
                ", UbanTime='" + UbanTime + '\'' +
                '}';
    }

    public int getUID() {
        return UID;
    }

    public void setUID(int UID) {
        this.UID = UID;
    }

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }

    public String getUpasswd() {
        return Upasswd;
    }

    public void setUpasswd(String upasswd) {
        Upasswd = upasswd;
    }

    public String getUgender() {
        return Ugender;
    }

    public void setUgender(String ugender) {
        Ugender = ugender;
    }

    public String getUint() {
        return Uint;
    }

    public void setUint(String uint) {
        Uint = uint;
    }

    public int getUfavorPrivacy() {
        return UfavorPrivacy;
    }

    public void setUfavorPrivacy(int ufavorPrivacy) {
        UfavorPrivacy = ufavorPrivacy;
    }

    public int getUcollPrivacy() {
        return UcollPrivacy;
    }

    public void setUcollPrivacy(int ucollPrivacy) {
        UcollPrivacy = ucollPrivacy;
    }

    public int getUbanLevel() {
        return UbanLevel;
    }

    public void setUbanLevel(int ubanLevel) {
        UbanLevel = ubanLevel;
    }

    public String getUbanTime() {
        return UbanTime;
    }

    public void setUbanTime(String ubanTime) {
        UbanTime = ubanTime;
    }
}
