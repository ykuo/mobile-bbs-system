package com.tust.androidbbsclient.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tust.androidbbsclient.R;
import com.tust.androidbbsclient.VisitorActivity;
import com.tust.androidbbsclient.tool.Tool;
import com.tust.androidbbsclient.trans.CommentDetail;

import java.util.List;

public class ArtDetailCommentRecyclerAdapter extends RecyclerView.Adapter<ArtDetailCommentViewHolder> {
    private Toast toast;
    private Context context;
    private Activity activity;
    private List<CommentDetail> commentDetails;

    public ArtDetailCommentRecyclerAdapter(Context context, Activity activity, List<CommentDetail> commentDetails) {
        this.context = context;
        this.activity = activity;
        this.commentDetails = commentDetails;
    }

    @NonNull
    @Override
    public ArtDetailCommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycleitem_artdetail_comdetail, parent, false);
        ArtDetailCommentViewHolder artDetailCommentViewHolder = new ArtDetailCommentViewHolder(view);

        return artDetailCommentViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ArtDetailCommentViewHolder holder, int position) {
        CommentDetail itemdata = commentDetails.get(position);

        //填充数据源
        holder.textViewComDetailUID.setText(String.valueOf(itemdata.getUID()));
        holder.textViewComDetailUname.setText(itemdata.getUname());
        holder.textViewComDetailComTime.setText(itemdata.getComTime());
        holder.textViewComDetailComContent.setText(itemdata.getComContent());

        //用户名文本监听事件
        holder.textViewComDetailUname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToast(Tool.addEmoji() + "正在打开“" + holder.textViewComDetailUname.getText().toString() + "”的访客空间！");

                //跳转活动
                Intent intent = new Intent(context, VisitorActivity.class);
                intent.putExtra("UID", Integer.parseInt(holder.textViewComDetailUID.getText().toString()));
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_out,R.anim.fade_in);//改变跳转活动动画，淡出淡入
            }
        });
    }

    @Override
    public int getItemCount() {
        return commentDetails.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    //Toast消息提示框显示方法
    private void showToast(String s) {
        if (toast == null){
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }else {
            toast.cancel();
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }
        toast.show();
    }
}

//ViewHolder类
class ArtDetailCommentViewHolder extends RecyclerView.ViewHolder {
    //数据视图组件
    TextView textViewComDetailUID;
    TextView textViewComDetailUname;
    TextView textViewComDetailComTime;
    TextView textViewComDetailComContent;

    public ArtDetailCommentViewHolder(@NonNull View itemView) {
        super(itemView);

        //绑定RecyclerView子项的控件
        textViewComDetailUID = itemView.findViewById(R.id.tv_comDetailUID);
        textViewComDetailUname = itemView.findViewById(R.id.tv_comDetailUname);
        textViewComDetailComTime = itemView.findViewById(R.id.tv_comDetailComTime);
        textViewComDetailComContent = itemView.findViewById(R.id.tv_comDetailComContent);
    }
}
