package com.tust.androidbbsclient.trans;

/**
 * 通信类：贴文列表
 * 属性：贴文ID 实型 自动增长 主键
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：贴文类别 变长字符20 非空
 * 属性：贴文标题 变长字符200 非空
 * 属性：贴文点赞量 实型 非空
 * 属性：贴文收藏量 实型 非空
 * 属性：贴文评论量 实型 非数据库
 * 属性：贴文发布时间 日期时间型 非空
 * 属性：贴文审核状态 小实型 非空 自定义约束（0or1or-1） 规定默认0待审核，1通过，-1不通过
 */
public class ArticleList {
    private int ArtID;
    private String Uname;
    private String ArtCategory;
    private String ArtTitle;
    private int ArtFavorNum;
    private int ArtCollNum;
    private int ArtComNum;
    private String ArtSubTime;
    private int ArtStatus;

    public ArticleList(int artID, String uname, String artCategory, String artTitle, int artFavorNum, int artCollNum, int artComNum, String artSubTime, int artStatus) {
        ArtID = artID;
        Uname = uname;
        ArtCategory = artCategory;
        ArtTitle = artTitle;
        ArtFavorNum = artFavorNum;
        ArtCollNum = artCollNum;
        ArtComNum = artComNum;
        ArtSubTime = artSubTime;
        ArtStatus = artStatus;
    }

    public ArticleList(int artID, String uname, String artCategory, String artTitle, int artFavorNum, int artCollNum, int artComNum) {
        ArtID = artID;
        Uname = uname;
        ArtCategory = artCategory;
        ArtTitle = artTitle;
        ArtFavorNum = artFavorNum;
        ArtCollNum = artCollNum;
        ArtComNum = artComNum;
    }

    @Override
    public String toString() {
        return "ArticleList{" +
                "ArtID=" + ArtID +
                ", Uname='" + Uname + '\'' +
                ", ArtCategory='" + ArtCategory + '\'' +
                ", ArtTitle='" + ArtTitle + '\'' +
                ", ArtFavorNum=" + ArtFavorNum +
                ", ArtCollNum=" + ArtCollNum +
                ", ArtComNum=" + ArtComNum +
                ", ArtSubTime='" + ArtSubTime + '\'' +
                ", ArtStatus=" + ArtStatus +
                '}';
    }

    public int getArtID() {
        return ArtID;
    }

    public void setArtID(int artID) {
        ArtID = artID;
    }

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }

    public String getArtCategory() {
        return ArtCategory;
    }

    public void setArtCategory(String artCategory) {
        ArtCategory = artCategory;
    }

    public String getArtTitle() {
        return ArtTitle;
    }

    public void setArtTitle(String artTitle) {
        ArtTitle = artTitle;
    }

    public int getArtFavorNum() {
        return ArtFavorNum;
    }

    public void setArtFavorNum(int artFavorNum) {
        ArtFavorNum = artFavorNum;
    }

    public int getArtCollNum() {
        return ArtCollNum;
    }

    public void setArtCollNum(int artCollNum) {
        ArtCollNum = artCollNum;
    }

    public int getArtComNum() {
        return ArtComNum;
    }

    public void setArtComNum(int artComNum) {
        ArtComNum = artComNum;
    }

    public String getArtSubTime() {
        return ArtSubTime;
    }

    public void setArtSubTime(String artSubTime) {
        ArtSubTime = artSubTime;
    }

    public int getArtStatus() {
        return ArtStatus;
    }

    public void setArtStatus(int artStatus) {
        ArtStatus = artStatus;
    }
}
