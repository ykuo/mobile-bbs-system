package com.tust.androidbbsclient.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DBNAME = "bbsclient.db";
    private static final int VERSION = 1;

    public DBHelper(@Nullable Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /*sqLiteDatabase.execSQL(
            "create table if not exists user(" +
                "UID int primary key," +
                "Uname varchar(20) not null unique," +
                "Upasswd varchar(10) not null," +
                "Ugender varchar(2)," +
                "Uint varchar(600)," +
                "UfavorPrivacy tinyint not null," +
                "UcollPrivacy tinyint not null," +
                "UbanLevel tinyint not null," +
                "UbanTime datetime)");*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
