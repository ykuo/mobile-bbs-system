package com.tust.androidbbsclient.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tust.androidbbsclient.ArticleDetailActivity;
import com.tust.androidbbsclient.R;
import com.tust.androidbbsclient.tool.Tool;
import com.tust.androidbbsclient.trans.CommentList;

import java.util.List;

public class PersonComListRecyclerAdapter extends RecyclerView.Adapter<PersonComListViewHolder> {
    private Toast toast;
    private Context context;
    private Activity activity;
    private List<CommentList> commentLists;
    public String Uname;

    public PersonComListRecyclerAdapter(Context context, Activity activity, List<CommentList> commentLists, String Uname) {
        this.context = context;
        this.activity = activity;
        this.commentLists = commentLists;
        this.Uname = Uname;
    }

    @NonNull
    @Override
    public PersonComListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycleitem_person_comlist, parent, false);
        PersonComListViewHolder personComListViewHolder = new PersonComListViewHolder(view);

        return personComListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonComListViewHolder holder, int position) {
        CommentList itemdata = commentLists.get(position);

        //填充数据源
        holder.textViewComListArtID.setText(String.valueOf(itemdata.getArtID()));
        holder.textViewComListUname.setText(String.format("%1$s：", Uname));
        holder.textViewComListArtTitle.setText(itemdata.getArtTitle());
        holder.textViewComListComTime.setText(itemdata.getComTime());
        holder.textViewComListComContent.setText(itemdata.getComContent());
        if (itemdata.getComStatus() == 0) {
            holder.textViewComListComStatus.setText("待审核");
            holder.textViewComListComStatus.setTextColor(ContextCompat.getColor(context, R.color.powderblue));
        }
        else if (itemdata.getComStatus() == 1) holder.textViewComListComStatus.setText("审核通过");
        else if (itemdata.getComStatus() == -1) {
            holder.textViewComListComStatus.setText("审核不通过");
            holder.textViewComListComStatus.setTextColor(ContextCompat.getColor(context, R.color.sandybrown));
        }

        //RecyclerView子项监听事件
        holder.linearLayoutComListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToast(Tool.addEmoji() + "正在打开贴文！");

                //跳转活动，带有ArtID参数
                Intent intent = new Intent(context, ArticleDetailActivity.class);
                intent.putExtra("ArtID", Integer.valueOf(holder.textViewComListArtID.getText().toString()));
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_out,R.anim.fade_in);//改变跳转活动动画，淡出淡入
            }
        });
    }

    @Override
    public int getItemCount() {
        return commentLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    //Toast消息提示框显示方法
    private void showToast(String s) {
        if (toast == null){
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }else {
            toast.cancel();
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }
        toast.show();
    }
}

//ViewHolder类
class PersonComListViewHolder extends RecyclerView.ViewHolder {
    //数据视图组件
    LinearLayout linearLayoutComListItem;
    TextView textViewComListArtID;
    TextView textViewComListUname;
    TextView textViewComListArtTitle;
    TextView textViewComListComTime;
    TextView textViewComListComContent;
    TextView textViewComListComStatus;

    public PersonComListViewHolder(@NonNull View itemView) {
        super(itemView);

        //绑定RecyclerView子项的控件
        linearLayoutComListItem = itemView.findViewById(R.id.ll_comListItem);
        textViewComListArtID = itemView.findViewById(R.id.tv_comListArtID);
        textViewComListUname = itemView.findViewById(R.id.tv_comListUname);
        textViewComListArtTitle = itemView.findViewById(R.id.tv_comListArtTitle);
        textViewComListComTime = itemView.findViewById(R.id.tv_comListComTime);
        textViewComListComContent = itemView.findViewById(R.id.tv_comListComContent);
        textViewComListComStatus = itemView.findViewById(R.id.tv_comListComStatus);
    }
}
