package com.tust.androidbbsclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tust.androidbbsclient.R;
import com.tust.androidbbsclient.tool.Tool;

public class ExploreCategoriesRecyclerAdapter extends RecyclerView.Adapter<ExploreCategoriesViewHolder> {
    private Context context;
    private Toast toast;
    private String[] categories;
    private OnItemClickListener onItemClickListener;

    //item点击事件接口类，其方法在碎片中实现
    public interface OnItemClickListener {
        void onItemClick(String category);
    }

    //item监听点击事件，置入包含onItemClick实现方法的Explore碎片
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //有参构造函数
    public ExploreCategoriesRecyclerAdapter(Context context, String[] categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public ExploreCategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycleitem_explore_categories, parent, false);
        ExploreCategoriesViewHolder exploreCategoriesViewHolder = new ExploreCategoriesViewHolder(view);

        return exploreCategoriesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExploreCategoriesViewHolder holder, int position) {
        String category = categories[position];

        //填充数据源
        holder.textViewCategoryListArtCategory.setText(category);

        //设置选中类别的样式
        if (category.equals(Tool.selectedCategory)) {
            holder.textViewCategoryListArtCategory.setTextColor(ContextCompat.getColor(context, R.color.coral));
            Tool.oldTextViewCategoryListArtCategory = holder.textViewCategoryListArtCategory;
        }
        else holder.textViewCategoryListArtCategory.setTextColor(ContextCompat.getColor(context, R.color.darkgrey));

        //类别文本监听事件
        holder.textViewCategoryListArtCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showToast(Tool.addEmoji() + "类别：" + holder.textViewCategoryListArtCategory.getText().toString());

                if (Tool.oldTextViewCategoryListArtCategory != null) Tool.oldTextViewCategoryListArtCategory.setTextColor(ContextCompat.getColor(view.getContext(), R.color.darkgrey));
                holder.textViewCategoryListArtCategory.setTextColor(ContextCompat.getColor(view.getContext(), R.color.coral));

                Tool.oldTextViewCategoryListArtCategory = holder.textViewCategoryListArtCategory;//储存为上一次点击的TextView，便于下次点击时还原样式
                Tool.selectedCategory = category;//储存当前选中类别
                if (onItemClickListener != null) onItemClickListener.onItemClick(category);//调用父碎片的onItemClick，并传递参数category
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    //Toast消息提示框显示方法
    private void showToast(String s) {
        if (toast == null){
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }else {
            toast.cancel();
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        }
        toast.show();
    }
}

//ViewHolder类
class ExploreCategoriesViewHolder extends RecyclerView.ViewHolder {
    //数据视图组件
    TextView textViewCategoryListArtCategory;

    public ExploreCategoriesViewHolder(@NonNull View itemView) {
        super(itemView);

        //绑定RecyclerView子项的控件
        textViewCategoryListArtCategory = itemView.findViewById(R.id.tv_categoryListArtCategory);
    }
}
