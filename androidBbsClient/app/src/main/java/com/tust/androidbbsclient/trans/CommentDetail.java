package com.tust.androidbbsclient.trans;

/**
 * 通信类 评论详情
 * 属性：用户ID 实型 非空 外键
 * 属性：用户名 变长字符20 非空 唯一
 * 属性：评论内容 变长字符600 非空
 * 属性：评论时间 日期时间型 非空
 */
public class CommentDetail {
    private int UID;
    private String Uname;
    private String ComContent;
    private String ComTime;

    public CommentDetail(int UID, String uname, String comContent, String comTime) {
        this.UID = UID;
        Uname = uname;
        ComContent = comContent;
        ComTime = comTime;
    }

    @Override
    public String toString() {
        return "CommentDetail{" +
                "UID=" + UID +
                ", Uname='" + Uname + '\'' +
                ", ComContent='" + ComContent + '\'' +
                ", ComTime='" + ComTime + '\'' +
                '}';
    }

    public int getUID() {
        return UID;
    }

    public void setUID(int UID) {
        this.UID = UID;
    }

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }

    public String getComContent() {
        return ComContent;
    }

    public void setComContent(String comContent) {
        ComContent = comContent;
    }

    public String getComTime() {
        return ComTime;
    }

    public void setComTime(String comTime) {
        ComTime = comTime;
    }
}
